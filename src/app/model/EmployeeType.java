package app.model;

/**
 * Created by arifk on 24.8.17.
 */
public class EmployeeType {
    private int typeId;
    private String typeName;
    private String note;

    public int getTypeId() {
        return typeId;
    }

    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }

    public String getName() {
        return typeName;
    }

    public void setName(String name) {
        this.typeName = name;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public String toString() {
        return typeName;
    }
}
