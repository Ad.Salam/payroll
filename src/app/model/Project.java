package app.model;

import java.util.List;

/**
 * Created by arifk on 23.10.17.
 */
public class Project {
    private int projectId;
    private int deptId;
    private int managerId;
    private Employee manager;
    private Department department;
    private List<ProjectDetails> projectDetails;
    private String projectTitle;
    private String projectDesc;
    private int hoursWorked;
    private int active;

    public int getProjectId() {
        return projectId;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }

    public int getDeptId() {
        return deptId;
    }

    public void setDeptId(int deptId) {
        this.deptId = deptId;
    }

    public String getProjectTitle() {
        return projectTitle;
    }

    public void setProjectTitle(String projectTitle) {
        this.projectTitle = projectTitle;
    }

    public String getProjectDesc() {
        return projectDesc;
    }

    public void setProjectDesc(String projectDesc) {
        this.projectDesc = projectDesc;
    }

    public int getHoursWorked() {
        return hoursWorked;
    }

    public void setHoursWorked(int hoursWorked) {
        this.hoursWorked = hoursWorked;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    public boolean isActive() {
        return active == 1;
    }

    public int getManagerId() {
        return managerId;
    }

    public void setManagerId(int managerId) {
        this.managerId = managerId;
    }

    public Employee getManager() {
        return manager;
    }

    public void setManager(Employee manager) {
        this.manager = manager;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    @Override
    public String toString() {
        return projectTitle;
    }

    public List<ProjectDetails> getProjectDetails() {
        return projectDetails;
    }

    public void setProjectDetails(List<ProjectDetails> projectDetails) {
        this.projectDetails = projectDetails;
    }
}
