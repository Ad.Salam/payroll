package app.model.hr_enum;

/**
 * Created by arifk on 24.8.17.
 */
public enum Gender {
    Male(1), Female(2), Other(3);
    public int val;

    Gender(int i) {
        val = i;
    }

    public static Gender intToEnum(int data) {
        switch (data) {
            case 1:
                return Male;
            case 2:
                return Female;
            default:
                return Other;
        }
    }
}
