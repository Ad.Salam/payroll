package app.model;

import app.model.hr_enum.Gender;

import java.sql.Date;
import java.util.List;

/**
 * Created by arifk on 24.8.17.
 */
public class Employee {
    private int empId;
    private int deptId;
    private int jobId;
    private int catId;
    private int typeId;
    private Job job;
    private Address address;
    private EmployeeType type;
    private EmployeeCategory category;
    private Department department;
    private String firstName;
    private String lastName;
    private Date dob;
    private String email;
    private int cellPhone;
    private int officePhone;
    private int gender;
    private int active;
    private String ssn;
    private String image;
    private String empCode;
    private Date lastLogin;
    private Date dateSignup;
    private Date createDate;
    private List<TimeSheet> attendance;

    public int getEmpId() {
        return empId;
    }

    public void setEmpId(int empId) {
        this.empId = empId;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public EmployeeType getType() {
        return type;
    }

    public void setType(EmployeeType type) {
        this.type = type;
    }

    public EmployeeCategory getCategory() {
        return category;
    }

    public void setCategory(EmployeeCategory category) {
        this.category = category;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public boolean isActive() {
        return active == 1;
    }

    public int getDepartmentId() {
        return deptId;
    }

    public void setDepartmentId(int departmentId) {
        this.deptId = departmentId;
    }

    public int getJobId() {
        return jobId;
    }

    public void setJobId(int jobId) {
        this.jobId = jobId;
    }

    public int getCatId() {
        return catId;
    }

    public void setCatId(int catId) {
        this.catId = catId;
    }

    public int getTypeId() {
        return typeId;
    }

    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getCellPhone() {
        return cellPhone;
    }

    public void setCellPhone(int cellPhone) {
        this.cellPhone = cellPhone;
    }

    public int getOfficePhone() {
        return officePhone;
    }

    public void setOfficePhone(int officePhone) {
        this.officePhone = officePhone;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    public String getSsn() {
        return ssn;
    }

    public void setSsn(String ssn) {
        this.ssn = ssn;
    }

    public String getEmpCode() {
        return empCode;
    }

    public void setEmpCode(String empCode) {
        this.empCode = empCode;
    }

    public Date getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(Date lastLogin) {
        this.lastLogin = lastLogin;
    }

    public Date getDateSignup() {
        return dateSignup;
    }

    public void setDateSignup(Date dateSignup) {
        this.dateSignup = dateSignup;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getFullName() {
        return getFirstName().concat(" ").concat(getLastName());
    }

    @Override
    public String toString() {
        return firstName + " " + lastName;
    }

    public List<TimeSheet> getAttendance() {
        return attendance;
    }

    public void setAttendance(List<TimeSheet> attendance) {
        this.attendance = attendance;
    }
}
