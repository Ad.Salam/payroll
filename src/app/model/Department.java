package app.model;

import java.util.List;

/**
 * Created by arifk on 24.8.17.
 */
public class Department {
    private int deptId;
    private int managerId;
    private int deptParentId;
    private Department parentDept;
    private Employee manager;
    private List<Employee> employees;
    private String deptName;
    private String location;
    private String deptDesc;

    public int getDeptId() {
        return deptId;
    }

    public void setDeptId(int deptId) {
        this.deptId = deptId;
    }

    public int getManagerId() {
        return managerId;
    }

    public void setManagerId(int managerId) {
        this.managerId = managerId;
    }

    public Employee getManager() {
        return manager;
    }

    public void setManager(Employee manager) {
        this.manager = manager;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDeptDesc() {
        return deptDesc;
    }

    public void setDeptDesc(String deptDesc) {
        this.deptDesc = deptDesc;
    }

    public Department getParentDept() {
        return parentDept;
    }

    public void setParentDept(Department parentDept) {
        this.parentDept = parentDept;
    }

    public int getDeptParentId() {
        return deptParentId;
    }

    public void setDeptParentId(int deptParentId) {
        this.deptParentId = deptParentId;
    }

    @Override
    public String toString() {
        return  deptName;
    }
}
