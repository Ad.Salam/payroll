package app.ui.employee;

import app.config.ConfigKeys;
import app.core.DB;
import app.core.DateSerializer;
import app.core.Role;
import app.model.*;
import app.model.hr_enum.Gender;
import app.modules.Common.Base;
import app.modules.Employee.AddEmployeePresenter;
import app.modules.Employee.EmployeeContract;
import app.ui.LoginView;
import app.util.Util;
import com.sun.istack.internal.Nullable;

import javax.swing.*;
import java.awt.event.ItemEvent;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.StringTokenizer;

/**
 * Created by arifk on 14.9.17.
 */
public class AddEmployeeView extends Base implements EmployeeContract.AddEmployeeView {
    private AddEmployeePresenter presenter;
    private JTextField tfFirstName;
    private JTextField tfLastName;
    private JTextField tfDay;
    private JComboBox cbCat;
    private JComboBox cbType;
    private JComboBox cbDepartment;
    private JComboBox cbJobName;
    private JTextField tfCellPhone;
    private JTextField tfOfficePhone;
    private JTextField tfEmail;
    private JTextField tfSSN;
    private JRadioButton maleRadioButton;
    private JRadioButton femaleRadioButton;
    private JButton submitButton;
    private JButton resetButton;
    private JTextField tfCity;
    private JTextField tfState;
    private JTextField tfZipCode;
    private JComboBox cbCountry;
    private JPanel addEmployeeView;
    private JTextArea tfAddress1;
    private JTextArea tfAddress2;
    private JButton selectImageButton;
    private JLabel empImage;
    private JScrollPane scrollPane;
    private JTextField tfMon;
    private JTextField tfYear;

    private List<EmployeeCategory> categories;
    private List<Department> departments;
    private List<EmployeeType> types;
    private List<Country> countries;
    List<Job> jobs;
    private int deptId = 0, catId = 0, typeId = 0, countryId = 0, jobId = 0;
    private Gender gender;
    private File selectedFile;
    private String imageTitle = "default_image.jpg";
    private int empId = 0;
    private Address empAddress = null;

    public AddEmployeeView(@Nullable Integer employeeId) {
        empId = employeeId != null ? employeeId : 0;
        setUi();
        if (isDbConnected()) {
            this.presenter = new AddEmployeePresenter(this);
            categories = presenter.getCategory();
            departments = presenter.getDepartment();
            types = presenter.getType();
            countries = presenter.getCountry();
            init();
        }
    }

    @Override
    public void setUi() {
        setTitle("Add New Employee");
        setUi(addEmployeeView);
    }

    @Override
    public void init() {
        selectImageButton.addActionListener(e -> {
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setDialogTitle("select an image");
            fileChooser.setCurrentDirectory(new File(System.getProperty("user.home")));
            int result = fileChooser.showOpenDialog(this);
            if (result == JFileChooser.APPROVE_OPTION) {
                selectedFile = fileChooser.getSelectedFile();
                ImageIcon icon = new ImageIcon(selectedFile.getAbsolutePath());
                if (icon.getIconWidth() > 150 || icon.getIconHeight() > 150)
                    message("Image too long ");
                else {
                    empImage.setIcon(icon);
                    imageTitle = selectedFile.getAbsoluteFile().getName();
                }
            }
        });
        ButtonGroup group = new ButtonGroup();
        group.add(maleRadioButton);
        group.add(femaleRadioButton);
        gender = Gender.Male;
        maleRadioButton.setSelected(true);
        maleRadioButton.addActionListener(e -> gender = Gender.Male);
        femaleRadioButton.addActionListener(e -> {
            gender = Gender.Female;
        });
        cbCat.addItem("-- select category --");
        for (EmployeeCategory category : categories) {
            cbCat.addItem(category);
        }
        cbDepartment.addItem("-- select department --");
        cbType.addItem("-- select type --");
        for (Department department : departments) {
            cbDepartment.addItem(department);
        }
        for (EmployeeType type : types) {
            cbType.addItem(type);
        }
        for (Country country : countries) {
            cbCountry.addItem(country.getCon_name());
        }


        cbDepartment.addActionListener(e -> departments.forEach(department -> {
            if (department == cbDepartment.getSelectedItem()) {
                deptId = department.getDeptId();
                jobs = presenter.getJobByDeptId(department.getDeptId());
                cbJobName.removeAllItems();
                cbJobName.addItem("-- select job --");
                for (Job job : jobs) {
                    cbJobName.addItem(job);
                }
            }
        }));

        cbCat.addItemListener(e -> {
            categories.forEach(category -> {
                if (category == cbCat.getSelectedItem())
                    catId = category.getCat_id();
            });
        });

        cbType.addActionListener(e -> types.forEach(tp -> {
                    if (tp == cbType.getSelectedItem())
                        typeId = tp.getTypeId();
                }
        ));
        cbCountry.addActionListener(e -> countries.forEach(cn -> {
            if (cn.getCon_name().equals(cbCountry.getSelectedItem()))
                countryId = cn.getId();
        }));
        cbJobName.addActionListener(e -> {
            for (Job job : jobs) {
                if (job == cbJobName.getSelectedItem()) {
                    jobId = job.getJobId();
                }
            }
        });
        submitButton.addActionListener(e -> {
            if (dataIsValid())
                generateEmployeeObj();
        });
        resetButton.addActionListener(e -> clearFields());
        if (empId != 0) {
            initViewData();
        }
    }

    private void initViewData() {
        Employee employee = presenter.getEmployee(empId);
        tfFirstName.setText(employee.getFirstName());
        tfLastName.setText(employee.getLastName());
        empAddress = employee.getAddress();
        tfZipCode.setText(String.valueOf(empAddress.getZip()));
        tfState.setText(empAddress.getState());
        tfDay.setText(String.valueOf(DateSerializer.getCalenderDate(employee.getDob()).get(Calendar.DAY_OF_MONTH)));
        tfMon.setText(String.valueOf(DateSerializer.getCalenderDate(employee.getDob()).get(Calendar.MONTH + 1)));
        tfYear.setText(String.valueOf(DateSerializer.getCalenderDate(employee.getDob()).get(Calendar.YEAR)));
        tfEmail.setText(employee.getEmail());
        tfCity.setText(empAddress.getCity());
        tfAddress2.setText(empAddress.getAddress2());
        tfAddress1.setText(empAddress.getAddress1());
        tfCellPhone.setText(String.valueOf(employee.getCellPhone()));
        tfOfficePhone.setText(String.valueOf(employee.getOfficePhone()));
        tfSSN.setText(employee.getSsn());
        int catIndex = 0, typeIndex = 0, deptIndex = 0, jobIndex = 0;
        for (EmployeeCategory cat : categories) {
            if (cat.getCat_id() == employee.getCatId()) {
                catIndex = categories.indexOf(cat) + 1;
            }
        }
        for (EmployeeType tp : types) {
            if (tp.getTypeId() == employee.getTypeId()) {
                typeIndex = types.indexOf(tp) + 1;
            }
        }
        for (Department dept : departments) {
            if (dept.getDeptId() == employee.getDepartmentId()) {
                deptIndex = departments.indexOf(dept) + 1;
            }
        }
        cbCat.setSelectedIndex(catIndex);
        cbType.setSelectedIndex(typeIndex);
        cbDepartment.setSelectedIndex(deptIndex);
        cbJobName.setSelectedItem(employee.getJob());
        imageTitle = employee.getImage();
        if (Gender.intToEnum(employee.getGender()) == Gender.Male) {
            maleRadioButton.setSelected(true);
        } else if (Gender.intToEnum(employee.getGender()) == Gender.Female) {
            femaleRadioButton.setSelected(true);
        }
        for (Job job : jobs) {
            if (job.getJobId() == employee.getJobId()) {
                jobIndex = jobs.indexOf(job) + 1;
            }
        }
        cbJobName.setSelectedIndex(jobIndex);
        empImage.setIcon(new ImageIcon(ConfigKeys.FILE_ROUTE + employee.getImage()));
    }

    private void clearFields() {
        tfFirstName.setText("");
        tfLastName.setText("");
        tfZipCode.setText("");
        tfState.setText("");
        tfDay.setText(String.valueOf(0));
        tfMon.setText(String.valueOf(0));
        tfYear.setText(String.valueOf(0));
        tfEmail.setText("");
        tfCity.setText("");
        tfAddress2.setText("");
        tfAddress1.setText("");
        tfCellPhone.setText("");
        tfOfficePhone.setText("");
        tfSSN.setText("");
        cbJobName.setSelectedIndex(0);
        cbCat.setSelectedIndex(0);
        cbType.setSelectedIndex(0);
        cbDepartment.setSelectedIndex(0);
        empImage.setIcon(null);

    }

    private boolean dataIsValid() {
        if (tfFirstName.getText().isEmpty()) {
            message("First name is required");
            return false;
        }
        if (tfLastName.getText().isEmpty()) {
            message("Last name is required");
            return false;
        }
        if (tfAddress1.getText().isEmpty()) {
            message("Present Address is required");
            return false;
        }
        if (tfAddress2.getText().isEmpty()) {
            message("Permanent Address is required");
            return false;
        }
        if (tfCellPhone.getText().isEmpty()) {
            message("Cell Phone is required");
            return false;
        }
        if (tfCity.getText().isEmpty()) {
            message("City is required");
            return false;
        }
        if (tfEmail.getText().isEmpty()) {
            message("Email is required");
            return false;
        } else {
            if (!Util.isValidEmail(tfEmail.getText())) {
                message("Please enter a valid email address !");
                return false;
            }
        }
        if (tfDay.getText().isEmpty()) {
            message("Date of birth is required");
            return false;
        }

        if (tfZipCode.getText().isEmpty()) {
            message("Zip Code is required");
            return false;
        }
        if (gender == null) {
            message("Gender is required");
            return false;
        }
        return true;
    }

    private void generateEmployeeObj() {
        Employee employee = new Employee();
        employee.setEmpId(empId);
        employee.setFirstName(tfFirstName.getText());
        employee.setLastName(tfLastName.getText());
        employee.setDepartmentId(deptId);
        employee.setCatId(catId);
        employee.setTypeId(typeId);
        employee.setJobId(jobId);
        employee.setEmail(tfEmail.getText());
        employee.setSsn(tfSSN.getText());
        employee.setImage(imageTitle);
        String dob = tfDay.getText().concat("-").concat(tfMon.getText()).concat("-").concat(tfYear.getText());
        employee.setDob(new Date(DateSerializer.getDate(dob).getTime()));
        employee.setCellPhone(Integer.parseInt(tfCellPhone.getText()));
        employee.setOfficePhone(tfOfficePhone.getText().isEmpty() ? 0 : Integer.parseInt(tfOfficePhone.getText()));
        employee.setActive(1);
        employee.setGender(gender.val);
        employee.setAddress(getAddress(empId));
        if (empId == 0)
            employee.setEmpCode(generateEmpCode());
        boolean isSuccess = presenter.addNewEmployee(employee);
        if (isSuccess) {
            if (selectedFile != null) {
                uploadImage();
            }
            message("Success ");
            if (currentUser == null) {
                this.dispose();
                new LoginView();
                return;
            }
            if (currentUser.getRole().getRoleId() == Role.Employee.val) {
                this.dispose();
                new EmployeeView();
            }
            clearFields();
        } else
            message("Failed !");
    }

    private String generateEmpCode() {
        Calendar calendar = Calendar.getInstance();
        return ConfigKeys.COMPANY_CODE.concat("_").concat(String.valueOf(tfDay.getText()).concat(String.valueOf(tfMon.getText())).concat(String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)).concat(String.valueOf(calendar.get(Calendar.MONTH))).concat(String.valueOf(calendar.get(Calendar.MINUTE)))));
    }

    private Address getAddress(int emId) {
        Address address = new Address();
        address.setAddressId(empAddress != null ? address.getAddressId() : 0);
        address.setZip(Integer.parseInt(tfZipCode.getText()));
        address.setCity(tfCity.getText());
        address.setState(tfState.getText());
        address.setAddress1(tfAddress1.getText());
        address.setAddress2(tfAddress2.getText());
        address.setEmp_id(emId);
        address.setCountryId(countryId);
        return address;
    }

    private void uploadImage() {
        try {
            byte[] file = new byte[1024];
            FileInputStream fRead = new FileInputStream(selectedFile.getAbsoluteFile());
            File outPut = new File(ConfigKeys.FILE_ROUTE + imageTitle);
            if (!outPut.getParentFile().exists()) {
                outPut.getParentFile().mkdir();
            }
            outPut.createNewFile();
            FileOutputStream otWrite = new FileOutputStream(outPut);
            while (fRead.read(file) != -1) {
                otWrite.write(file);
            }
            fRead.close();
            otWrite.close();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }
}
