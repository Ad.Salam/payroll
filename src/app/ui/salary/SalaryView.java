package app.ui.salary;

import app.core.DateSerializer;
import app.model.Employee;
import app.model.Payroll;
import app.model.Salary;
import app.model.TimeSheet;
import app.modules.Common.Base;
import app.modules.Salary.SalaryContract;
import app.modules.Salary.SalaryPresenter;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by arifk on 11.12.17.
 */
public class SalaryView extends Base implements SalaryContract.salaryView {

    private JTabbedPane tabbedPane1;
    private JPanel panel1;
    private JButton paymentButton;
    private JComboBox cmbSalaryEmp;
    private JComboBox cmbSalaryYear;
    private JButton saveButton;
    private JTextField txtAmont;
    private JTextField txtNote;
    private JTextField txtHoursWorked;
    private JTextField txtGrossPay;
    private JComboBox cmbPayEmployee;
    private JTextField txtDeduction;
    private JButton payButton;
    private JComboBox cmbPayYear;
    private JTextField txtNetPay;
    private JComboBox cmbPayDay;
    private JComboBox cmbPayMonth;
    private JButton backButton;
    private JTable tblSalaryList;
    private JButton salariesButton;
    private JButton backBtnSalarylist;
    private JComboBox cmbSearchEmployee;
    private JTable tblPaiedList;
    private JButton paidSalariesButton;
    private JButton btnBackFromPaid;
    private JComboBox cmbSearchPaidSalByEmp;
    private JComboBox cmbPaidSalarySrchMonth;
    private JComboBox cmbPaidSalarySrchYear;
    private JButton searchButton;
    private JButton backButton2;
    private SalaryPresenter presenter;

    private Salary salary;

    public SalaryView() {
        this.presenter = new SalaryPresenter(this);
        setUi();
        init();
    }

    @Override
    public void setUi() {
        setTitle("Salary");
        setUi(panel1);
    }

    @Override
    public void init() {
        presenter.getSalaryList();
        presenter.getActiveEmployee();
        saveButton.addActionListener(e -> {
            if (cmbSalaryEmp.getSelectedIndex() == 0) {
                message("Please select employee !");
                return;
            }
            if (txtAmont.getText().isEmpty()) {
                message("Amount is required !");
                return;
            }
            Employee employee = (Employee) cmbSalaryEmp.getSelectedItem();
            Salary prevSalary = presenter.getSalaryByEmpId(employee.getEmpId(), Integer.parseInt(cmbSalaryYear.getSelectedItem().toString()));


            Salary salary = new Salary();
            salary.setAmount(Double.parseDouble(txtAmont.getText()));
            salary.setEmpId(employee.getEmpId());
            salary.setNote(txtNote.getText());
            salary.setYear(Integer.parseInt(cmbSalaryYear.getSelectedItem().toString()));
            if (prevSalary != null) {
                int dialogResult = JOptionPane.showConfirmDialog(this, "Already added ! Do you want to update ?");
                if (dialogResult == JOptionPane.YES_OPTION) {
                    salary.setSalaryId(prevSalary.getSalaryId());
                    int id = presenter.updateSalary(salary);
                    if (id != 0) {
                        message("Salary Updated ");
                        tabbedPane1.setSelectedIndex(2);
                        presenter.getSalaryList();
                        return;
                    } else {
                        message("Failed !");
                        return;
                    }
                }
            }
            int id = presenter.insertSalary(salary);
            if (id > 0) {
                message("Salary added ");
                tabbedPane1.setSelectedIndex(2);
                presenter.getSalaryList();
            } else {
                message("Failed !");
            }

        });
        paymentButton.addActionListener(e -> {
            tabbedPane1.setSelectedIndex(1);

        });
        backButton.addActionListener(e -> {
            tabbedPane1.setSelectedIndex(0);
        });
        backBtnSalarylist.addActionListener(e -> {
            tabbedPane1.setSelectedIndex(0);
        });
        salariesButton.addActionListener(e -> {
            tabbedPane1.setSelectedIndex(2);
            presenter.getSalaryList();
        });
        cmbSearchEmployee.addActionListener(e -> {
            try {
                Employee employee = (Employee) cmbSearchEmployee.getSelectedItem();
                presenter.getSalaryList(employee.getEmpId());
            } catch (ClassCastException | NullPointerException ex) {
                ex.printStackTrace();
            }
        });
        cmbPayEmployee.addActionListener(e -> {
            try {
                txtGrossPay.setText("");
                Employee employee = (Employee) cmbPayEmployee.getSelectedItem();
                salary = presenter.getSalaryByEmpId(employee.getEmpId(), DateSerializer.getCalenderDate(new Date()).get(Calendar.YEAR));

                if (salary != null) {
                    txtGrossPay.setText(String.valueOf(salary.getAmount()));
                    txtNetPay.setText(String.valueOf(salary.getAmount()));
                } else {
                    message("Please add salary first !");
                    cmbPayEmployee.setSelectedIndex(0);
                    tabbedPane1.setSelectedIndex(0);
                }
            } catch (ClassCastException | NullPointerException ex) {
                ex.printStackTrace();
            }
        });
        txtDeduction.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {

                Double gp = Double.parseDouble(txtGrossPay.getText());
                int dd = Integer.parseInt(txtDeduction.getText());
                txtNetPay.setText(String.valueOf(gp - dd));
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                if (!txtGrossPay.getText().isEmpty()) {
                    Double gp = Double.parseDouble(txtGrossPay.getText());
                    int dd = Integer.parseInt(txtDeduction.getText());
                    txtNetPay.setText(String.valueOf(gp - dd));
                }
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
            }
        });
        payButton.addActionListener(e -> {
            calculateAndUpdate();
        });
        paidSalariesButton.addActionListener(e -> {
            tabbedPane1.setSelectedIndex(3);
            presenter.getPayList();
            for (Integer i = 2017; i <= 2025; i++) {
                cmbPaidSalarySrchYear.addItem(String.valueOf(i));
            }

            for (Integer i = 0; i <= 12; i++) {
                cmbPaidSalarySrchMonth.addItem(String.valueOf(i));
            }
        });
        btnBackFromPaid.addActionListener(e -> {
            tabbedPane1.setSelectedIndex(0);
        });
        searchButton.addActionListener(e -> {
            try {
                Employee employee = null;
                if (cmbSearchPaidSalByEmp.getSelectedIndex() != 0) {
                    employee = (Employee) cmbSearchPaidSalByEmp.getSelectedItem();
                }
                presenter.getPayList(employee == null ? null : employee.getEmpId(), Integer.parseInt(cmbPaidSalarySrchYear.getSelectedItem().toString()), Integer.parseInt(cmbPaidSalarySrchMonth.getSelectedItem().toString()));
            } catch (ClassCastException | NullPointerException ex) {
                ex.printStackTrace();
            }
        });
    }

    private void calculateAndUpdate() {
        if (cmbPayEmployee.getSelectedIndex() == 0) {
            message("Please select employee !");
            return;
        }
        Employee employee = (Employee) cmbPayEmployee.getSelectedItem();
        Payroll payroll = new Payroll();
        String d = cmbPayDay.getSelectedItem().toString().concat("-").concat(cmbPayMonth.getSelectedItem().toString()).concat("-").concat(cmbPayYear.getSelectedItem().toString());
        payroll.setDate(DateSerializer.getDateL(d, null));
        payroll.setDeductions(Double.parseDouble(txtDeduction.getText()));
        payroll.setNetPay(Double.parseDouble(txtNetPay.getText()));
        payroll.setGrossPay(Double.parseDouble(txtGrossPay.getText()));
        payroll.setHoursWorked(Integer.parseInt(txtHoursWorked.getText()));
        payroll.setEmpId(employee.getEmpId());
        Payroll prevPayroll = presenter.getPayrollByEmpId(employee.getEmpId(), Integer.parseInt(cmbPayYear.getSelectedItem().toString()), Integer.parseInt(cmbPayMonth.getSelectedItem().toString()));
        if (prevPayroll != null) {
            int dialogResult = JOptionPane.showConfirmDialog(this, "Already added ! Do you want to update ?");
            if (dialogResult == JOptionPane.YES_OPTION) {
                payroll.setPayrollId(prevPayroll.getPayrollId());
                int id = presenter.updatePayroll(payroll);
                if (id != 0) {
                    message("Updated ");
                    tabbedPane1.setSelectedIndex(2);
                    presenter.getPayList();
                    return;
                } else {
                    message("Failed !");
                    return;
                }
            }
        }
        int id = presenter.insertPayroll(payroll);
        if (id > 0) {
            message("Success");
            tabbedPane1.setSelectedIndex(3);
            presenter.getPayList();
        } else {
            message("Failed  !");
        }
    }

    @Override
    public void setEmployeeToView(List<Employee> employees) {
        cmbSearchPaidSalByEmp.removeAllItems();
        cmbSearchEmployee.removeAllItems();
        cmbPayEmployee.removeAllItems();
        cmbSalaryEmp.removeAllItems();
        cmbSearchPaidSalByEmp.addItem("-- select employee --");
        cmbSalaryEmp.addItem("-- select employee --");
        cmbPayEmployee.addItem("-- select employee --");
        cmbSearchEmployee.addItem("-- select employee --");
        for (Employee employee : employees) {
            cmbSalaryEmp.addItem(employee);
            cmbPayEmployee.addItem(employee);
            cmbSearchPaidSalByEmp.addItem(employee);
            cmbSearchEmployee.addItem(employee);
        }
        for (Integer i = 2017; i <= 2025; i++) {
            cmbSalaryYear.addItem(String.format("%02d", i));
            cmbPayYear.addItem(String.format("%02d", i));
        }

        for (Integer i = 0; i <= 31; i++) {
            if (i < 12) {
                cmbPayMonth.addItem(String.format("%02d", i + 1));
            }
            cmbPayDay.addItem(String.format("%02d", i));
        }
    }

    @Override
    public void setSalariesToView(List<Salary> salaries) {
        String columnNames[] = {"Name", "Year", "Salary"};
        DefaultTableModel model = new DefaultTableModel(columnNames, 0);
        for (Salary em : salaries) {
            Object[] data = {
                    em.getEmployee().getFullName(),
                    em.getYear(),
                    em.getAmount()
            };
            model.addRow(data);
        }
        tblSalaryList.setModel(model);
        tblSalaryList.setGridColor(Color.CYAN);
        tblSalaryList.setRowMargin(2);
        tblSalaryList.getColumnModel().getColumn(0).setMinWidth(200);
    }

    @Override
    public void setPayListToView(List<Payroll> payList) {
        String columnNames[] = {"Name", "Date", "Hours worked", "Gross Paid", "Deduct", "Net Paid"};
        DefaultTableModel model = new DefaultTableModel(columnNames, 0);
        for (Payroll pay : payList) {
            Object[] data = {
                    pay.getEmployee().getFullName(),
                    pay.getDate().toString(),
                    pay.getHoursWorked(),
                    pay.getGrossPay(),
                    pay.getDeductions(),
                    pay.getNetPay()
            };
            model.addRow(data);
        }
        tblPaiedList.setModel(model);
        tblPaiedList.setGridColor(Color.CYAN);
        tblPaiedList.setRowMargin(2);
        tblPaiedList.getColumnModel().getColumn(0).setMinWidth(200);
    }
}
