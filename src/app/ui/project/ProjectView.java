package app.ui.project;

import app.core.DateSerializer;
import app.model.Department;
import app.model.Employee;
import app.model.Project;
import app.model.ProjectDetails;
import app.modules.Common.Base;
import app.modules.Project.ProjectContract;
import app.modules.Project.ProjectPresenter;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.Date;
import java.util.List;

/**
 * Created by arifk on 6.11.17.
 */
public class ProjectView extends Base implements ProjectContract.ProjectView {

    private JPanel projectPanel;
    private JButton addProjectButton;
    private JButton assignProjectButton;
    private JButton projectListButton;
    private JButton showMembersButton;
    private JTabbedPane tabbedPane1;
    private JTable tblProjectList;
    private JButton saveProjectButton;
    private JTextField textProjectName;
    private JComboBox cmbDeprtment;
    private JComboBox cmbEmployeeList;
    private JTextArea textProjectDesc;
    private JCheckBox projctActiveCheckBox;
    private JComboBox cmbProject;
    private JComboBox cmbMember;
    private JButton assignButton;
    private JComboBox cmbProjectForGetAssiged;
    private JTable tableAssignedMember;
    private ProjectContract.ProjectPresenter presenter;

    public ProjectView() {
        presenter = new ProjectPresenter(this);
        setUi();
        init();
    }

    @Override
    public void setUi() {
        setUi(projectPanel);
        setTitle("Project");
    }

    @Override
    public void init() {
        presenter.getDepartmentList();
        presenter.getManagerList();
        presenter.getProjectList();
        saveProjectButton.addActionListener(e -> createProject());
        projectListButton.addActionListener(e -> {
            tabbedPane1.setSelectedIndex(0);
            presenter.getProjectList();
        });
        addProjectButton.addActionListener(e -> tabbedPane1.setSelectedIndex(1));
        assignProjectButton.addActionListener(e -> tabbedPane1.setSelectedIndex(2));
        assignButton.addActionListener(e -> assignProject());
        showMembersButton.addActionListener(e -> tabbedPane1.setSelectedIndex(3));
        cmbProjectForGetAssiged.addActionListener(e -> {
            try {
                presenter.getAssignedMembersList(((Project) cmbProjectForGetAssiged.getSelectedItem()).getProjectId());
            } catch (ClassCastException | NullPointerException ne) {
                ne.printStackTrace();
            }

        });

    }

    private void assignProject() {
        ProjectDetails details = new ProjectDetails();
        try {
            details.setProjectId(((Project) cmbProject.getSelectedItem()).getProjectId());
        } catch (ClassCastException ce) {
            message("Project is required !");
            return;
        }
        try {
            details.setEmpId(((Employee) cmbMember.getSelectedItem()).getEmpId());
        } catch (ClassCastException ce) {
            message("Member is required !");
            return;
        }

        details.setJoinDate(new Date());
        int id = presenter.assignProject(details);
        if (id == 0) {
            message("Failed to assign project !");
        } else {
            message("Project assigned !");
        }
    }

    private void createProject() {
        Project project = new Project();
        if (textProjectName.getText().isEmpty()) {
            message("Project name is required !");
            return;
        }
        project.setProjectTitle(textProjectName.getText());
        project.setProjectDesc(textProjectDesc.getText());
        try {
            project.setDeptId(((Department) cmbDeprtment.getSelectedItem()).getDeptId());

        } catch (ClassCastException e) {
            message("Department is required !");
            return;
        }
        try {
            project.setManagerId(((Employee) cmbEmployeeList.getSelectedItem()).getEmpId());
        } catch (ClassCastException e) {
            message("Manager is required !");
            return;
        }

        project.setActive(projctActiveCheckBox.isSelected() ? 1 : 0);
        int id = presenter.addProject(project);
        if (id == 0) {
            message("Failed to save project !");
        } else
            message("Success");
    }

    @Override
    public void setMangerListToView(List<Employee> employeeList) {
        cmbEmployeeList.addItem("-- select employee --");
        employeeList.forEach(employee -> cmbEmployeeList.addItem(employee));
        createMemberListForAssignToProject(employeeList);
    }

    private void createMemberListForAssignToProject(List<Employee> employeeList) {
        cmbMember.addItem("-- select member -- ");
        employeeList.forEach(employee -> {
            if (employee.isActive()) {
                cmbMember.addItem(employee);
            }
        });
    }

    @Override
    public void setDepartmentListToView(List<Department> departments) {
        cmbDeprtment.addItem("-- select department--");
        departments.forEach(department -> cmbDeprtment.addItem(department));
    }

    @Override
    public void setProjectListToView(List<Project> projectList) {
        if (projectList != null) {
            createProjectDropDown(projectList);
            String columnNames[] = {"Project Name", "Manager", "Department", "Description", "Active"};
            DefaultTableModel model = new DefaultTableModel(columnNames, 0);

            for (Project project : projectList) {
                Object[] data = {project.getProjectTitle(), project.getManager() != null ? project.getManager().getFullName() : "", project.getDepartment().getDeptName(),
                        project.getProjectDesc(), project.getActive() == 1 ? "Active" : "InActive"};
                model.addRow(data);
            }
            tblProjectList.setModel(model);
            tblProjectList.setGridColor(Color.CYAN);
            tblProjectList.setRowMargin(2);
        }
    }

    private void createProjectDropDown(List<Project> projects) {
        // cmbProject.removeAllItems();
        cmbProjectForGetAssiged.removeAllItems();
        cmbProject.addItem("-- select project --");
        cmbProjectForGetAssiged.addItem("-- select project --");
        projects.forEach(project -> {
            if (project.isActive()) {
                cmbProject.addItem(project);
                cmbProjectForGetAssiged.addItem(project);
            }
        });
    }

    @Override
    public void setAssignedMembersToView(List<ProjectDetails> details) {
        if (details != null) {
            String columnNames[] = {"Project Name", "Employee", "Join Date"};
            DefaultTableModel model = new DefaultTableModel(columnNames, 0);

            for (ProjectDetails projectDetails : details) {
                Object[] data = {projectDetails.getProject() != null ? projectDetails.getProject().getProjectTitle() : "", projectDetails.getEmployee() != null ? projectDetails.getEmployee().getFullName() : "", projectDetails.getJoinDate() == null ? "" : DateSerializer.getDateStringFromDate(projectDetails.getJoinDate())};
                model.addRow(data);
            }
            tableAssignedMember.setModel(model);
            tableAssignedMember.setGridColor(Color.CYAN);
            tableAssignedMember.setRowMargin(2);
        } else {
            message("No Member is assigned in this project !");
        }
    }
}
