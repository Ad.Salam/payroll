package app.ui.attendance;

import app.core.DateSerializer;
import app.model.Employee;
import app.model.Project;
import app.model.TimeSheet;
import app.modules.Attendence.AttendanceContract;
import app.modules.Attendence.AttendancePresenter;
import app.modules.Common.Base;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import java.util.List;

/**
 * Created by arifk on 23.10.17.
 */
public class AttendanceView extends Base implements AttendanceContract.View {

    private JPanel attendancePanel;
    private JComboBox cmbEmployee;
    private JComboBox cmbProject;
    private JComboBox cmbInHour;
    private JComboBox cmbInMinute;
    private JComboBox cmbOutHour;
    private JComboBox cmbOutMinute;
    private JCheckBox ckChecked;
    private JButton btnSubmit;
    private JTextArea txtWorkDesc;
    private JTabbedPane tabbedPane1;
    private JPanel pnlTakeAttendance;
    private JButton attendanceListButton;
    private JPanel pnlAttendanceList;
    private JTable tblAttendanceList;
    private JButton takeAttendanceButton;
    private JComboBox cmbInTimeType;
    private JComboBox cmbOutTimeType;
    private JRadioButton checkIn;
    private JRadioButton checkOUT;
    private JPanel panelCheckIn;
    private JPanel panelCheckout;
    private JLabel tvDesc;
    private JLabel checkInTime;
    private JButton attendanceReportButton;
    private JComboBox cmbAtYear;
    private JComboBox cmbAtMonth;
    private JComboBox cmbAtDay;
    private JTable tableAttendanceReport;
    private JComboBox cmbAtrEmployee;
    private JButton backButton;
    private JButton searchButton;
    private JTable tblTotalAttendnce;
    private JButton backTotalAt;
    private JComboBox cmbTotalYear;
    private JComboBox cmbTotalMonth;
    private JButton attendanceByMonthButton;
    private JButton searchButton1;
    private AttendanceContract.Presenter presenter;
    private List<Employee> employees;
    private List<Project> projects;

    public AttendanceView() {
        presenter = new AttendancePresenter(this);
        setUi();
        init();
    }

    @Override
    public void setUi() {
        setTitle("Attendance");
        setUi(attendancePanel);
    }

    @Override
    public void init() {
        presenter.getLastAttendanceList();
        attendancePanel.setBackground(Color.WHITE);
        attendanceListButton.addActionListener(e -> {
            tabbedPane1.setSelectedIndex(0);
            presenter.getLastAttendanceList();

        });
        takeAttendanceButton.addActionListener(e -> {
            tabbedPane1.setSelectedIndex(1);
            presenter.getEmployees();
            presenter.getProjects();
        });
        attendanceReportButton.addActionListener(e -> {
            presenter.getEmployees();
            tabbedPane1.setSelectedIndex(2);
            for (Integer i = 2017; i <= 2025; i++) {
                cmbAtYear.addItem(String.format("%02d", i));

            }
            for (Integer i = 0; i <= 31; i++) {
                if (i < 12) {
                    cmbAtMonth.addItem(String.format("%02d", i + 1));

                }
                cmbAtDay.addItem(String.format("%02d", i));
            }
        });
        backButton.addActionListener(e -> {
            tabbedPane1.setSelectedIndex(0);
        });
        backTotalAt.addActionListener(e -> {
            tabbedPane1.setSelectedIndex(0);
        });
        attendanceByMonthButton.addActionListener(e -> {
            tabbedPane1.setSelectedIndex(3);
            cmbTotalYear.removeAllItems();
            for (Integer i = 2017; i <= 2025; i++) {
                cmbTotalYear.addItem(String.format("%02d", i));
            }
            cmbTotalMonth.removeAllItems();
            for (Integer i = 1; i <= 12; i++) {
                cmbTotalMonth.addItem(String.format("%02d", i));

            }
        });
        for (Integer i = 1; i <= 60; i++) {
            if (i <= 12) {
                cmbInHour.addItem(String.format("%02d", i));
                cmbOutHour.addItem(String.format("%02d", i));
            }
            cmbInMinute.addItem(String.format("%02d", i));
            cmbOutMinute.addItem(String.format("%02d", i));

        }
        cmbInTimeType.addItem("AM");
        cmbInTimeType.addItem("PM");
        cmbOutTimeType.addItem("AM");
        cmbOutTimeType.addItem("PM");
        btnSubmit.addActionListener(e -> {
            submitAttendance();
        });
        ButtonGroup group = new ButtonGroup();
        group.add(checkIn);
        group.add(checkOUT);
        checkIn.setSelected(true);
        panelCheckIn.setVisible(false);
        checkIn.addChangeListener(e -> {
            if (checkIn.isSelected()) {
                panelCheckIn.setVisible(true);
                panelCheckout.setVisible(false);
                cmbProject.setVisible(true);
                txtWorkDesc.setVisible(true);
                tvDesc.setVisible(true);
            }
        });
        checkOUT.addChangeListener(e -> {
            if (checkOUT.isSelected()) {
                panelCheckIn.setVisible(false);
                panelCheckout.setVisible(true);
                cmbProject.setVisible(false);
                txtWorkDesc.setVisible(false);
                tvDesc.setVisible(false);
            }
        });
        searchButton.addActionListener(e -> {
            int y = Integer.parseInt(cmbAtYear.getSelectedItem().toString());
            int m = Integer.parseInt(cmbAtMonth.getSelectedItem().toString());
            int d = Integer.parseInt(cmbAtDay.getSelectedItem().toString());
            int empId = 0;
            try {
                empId = ((Employee) cmbAtrEmployee.getSelectedItem()).getEmpId();
            } catch (ClassCastException ce) {
//                message("Please select employee !");
//                return;
            }
            presenter.searchAttendanceReport(empId, y, m, d);
        });
        searchButton1.addActionListener(e -> {
            presenter.getAttendanceListByMonth(Integer.parseInt(cmbTotalYear.getSelectedItem().toString()), Integer.parseInt(cmbTotalMonth.getSelectedItem().toString()));
        });

    }

    
    private void submitAttendance() {
        TimeSheet sheet = new TimeSheet();
        //sheet.setChecked(ckChecked.isSelected() ? 1 : 0);

        sheet.setAttendDate(Calendar.getInstance().getTime());
        if (checkIn.isSelected())
            sheet.setCheckIn(DateSerializer.generateTime(Integer.parseInt(cmbInHour.getSelectedItem().toString()), Integer.parseInt(cmbInMinute.getSelectedItem().toString()), cmbInTimeType.getSelectedItem().toString()));
        if (cmbEmployee.getSelectedIndex() > 0) {
            sheet.setEmpId(((Employee) cmbEmployee.getSelectedItem()).getEmpId());
        } else {
            message("Employee is required !");
            return;
        }
        if (cmbProject.getSelectedIndex() > 0)
            sheet.setProjectId(projects.get(cmbProject.getSelectedIndex() - 1).getProjectId());
        else {
            if (!checkOUT.isSelected()) {
                message("Project is required !");
                return;
            }
        }
        sheet.setWorkDesc(txtWorkDesc.getText());
        TimeSheet checkTime = presenter.getAttendance(sheet.getEmpId(), new Date());

        if (checkTime != null) {
            sheet = checkTime;
            checkInTime.setText("Checked IN :  " + checkTime.getCheckIn().toString());
            if (!checkOUT.isSelected()) {
                message("Select checkout");
                checkOUT.setSelected(true);
                return;
            }
            sheet.setCheckOut(DateSerializer.generateTime(Integer.parseInt(cmbOutHour.getSelectedItem().toString()), Integer.parseInt(cmbOutMinute.getSelectedItem().toString()), cmbOutTimeType.getSelectedItem().toString()));
        }
        presenter.addAttendance(sheet);
    }

    @Override
    public void setEmployeeListToView(List<Employee> employeeList) {
        employees = employeeList;
        cmbEmployee.removeAllItems();
        cmbAtrEmployee.removeAllItems();
        cmbEmployee.addItem("-- select employee --");
        cmbAtrEmployee.addItem("-- select employee --");

        for (Employee employee : employeeList) {

            if (tabbedPane1.getSelectedIndex() == 1) {
                if (employee.isActive()) {
                    cmbEmployee.addItem(employee);
                }
            } else {
                cmbAtrEmployee.addItem(employee);
            }
        }

    }

    @Override
    public void setProjectsToView(List<Project> projects) {
        this.projects = projects;
        cmbProject.addItem("-- select project --");
        projects.forEach(project -> cmbProject.addItem(project.getProjectTitle()));
    }

    @Override
    public void success(Object obj) {
        if (obj instanceof String) message(obj.toString());
        tabbedPane1.setSelectedIndex(0);
        presenter.getLastAttendanceList();
    }

    @Override
    public void error(Object error) {
        message(error.toString());
    }

    @Override
    public void setAttendanceListToView(List<TimeSheet> timeSheets) {
        String columnNames[] = {"Employee Name", "In Time", "Out Time", "Working Project"};
        DefaultTableModel model = new DefaultTableModel(columnNames, 0);
        for (TimeSheet em : timeSheets) {
            Object[] data = {String.valueOf(em.getEmployee().getFullName()),
                    DateSerializer.getTimeStringFromTime(em.getCheckIn().toString()),
                    DateSerializer.getTimeStringFromTime(String.valueOf(em.getCheckOut())),
                    em.getProject().getProjectTitle()
            };
            model.addRow(data);
        }

        tblAttendanceList.setModel(model);
        tblAttendanceList.setGridColor(Color.CYAN);
        tblAttendanceList.setRowMargin(2);

    }

    @Override
    public void setAttendanceReportToView(List<TimeSheet> timeSheets) {
        if (timeSheets.size() == 0) {
            message("No attendance found !");
            return;
        }
        String columnNames[] = {"Day", "Name", "In Time", "Out Time", "Working Project"};
        DefaultTableModel model = new DefaultTableModel(columnNames, 0);
        for (TimeSheet em : timeSheets) {
            Object[] data = {
                    DateSerializer.getCalenderDate(em.getAttendDate()).get(Calendar.DAY_OF_MONTH),
                    em.getEmployee() != null ? em.getEmployee().getFullName() : "",
                    DateSerializer.getTimeStringFromTime(em.getCheckIn().toString()),
                    DateSerializer.getTimeStringFromTime(String.valueOf(em.getCheckOut())),
                    em.getProject() != null ? em.getProject().getProjectTitle() : ""
            };
            model.addRow(data);
        }
        tableAttendanceReport.setModel(model);
        tableAttendanceReport.setGridColor(Color.CYAN);
        tableAttendanceReport.setRowMargin(2);

    }

    Set<Integer> days = new LinkedHashSet<>();

    @Override
    public void setAttendanceByMonthListToView(List<Employee> employees) {
        if (employees.size() == 0) {
            message("No attendance found !");
            return;
        }

        String columnNames[] = new String[32];
        for (int i = 0; i <= 31; i++) {
            days.add(i + 1);
            if (i == 0) {
                columnNames[0] = "Name";
            } else
                columnNames[i] = String.valueOf(i);
        }

        DefaultTableModel model = new DefaultTableModel(columnNames, 0);
        for (Employee em : employees) {
            Object[] data = {
                    em.getLastName(),
                    "",
                    ""
            };
            model.addRow(data);
        }
        tblTotalAttendnce.setModel(model);
        tblTotalAttendnce.setGridColor(Color.CYAN);
        tblTotalAttendnce.setRowMargin(2);
        tblTotalAttendnce.getColumnModel().getColumn(0).setMinWidth(200);
        int row = 0;

        for (Employee employee : employees) {
            for (Integer i : days) {
                for (TimeSheet sheet : employee.getAttendance()) {
                    if (i == DateSerializer.getCalenderDate(sheet.getAttendDate()).get(Calendar.DAY_OF_MONTH)) {
                        model.setValueAt("H", row, i);
                    }
                }
            }
            row++;
        }
    }


}