package app.core;

import app.model.User;

import java.util.Date;

/**
 * Created by arifk on 8.11.17.
 */
public class UserSession {
    private int userSessionId;
    private int userId;
    private User user;
    private Date startLogin;
    private Date logOutTime;
    private int isLogin;

    public UserSession() {
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public boolean isAuth() {
        return isLogin == 1;
    }

    public int getIsLogin() {
        return isLogin;
    }

    public void setAuth(boolean auth) {
        isLogin = auth ? 1 : 0;
    }

    public Date getStartLogin() {
        return startLogin;
    }

    public void setStartLogin(Date startLogin) {
        this.startLogin = startLogin;
    }

    public int getUserSessionId() {
        return userSessionId;
    }

    public void setUserSessionId(int userSessionId) {
        this.userSessionId = userSessionId;
    }

    public Date getLogOutTime() {
        return logOutTime;
    }

    public void setLogOutTime(Date logOutTime) {
        this.logOutTime = logOutTime;
    }
}
