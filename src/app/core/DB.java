package app.core;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 * Created by arifk on 24.8.17.
 */
public class DB {
    private static String url = "jdbc:mysql://localhost:3306/hr_payroll";
    private static String driverName = "com.mysql.jdbc.Driver";
    private static String username = "root";
    private static String password = "";
    private static Connection con;

    public static Connection getConnection() {
        try {
            Class.forName(driverName);
            con = DriverManager.getConnection(url, username, password);
        } catch (Exception ex) {
            System.out.println("Driver not found.");
        }
        return con;
    }
}
