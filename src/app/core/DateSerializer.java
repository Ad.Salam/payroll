package app.core;

import com.sun.istack.internal.Nullable;

import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by arifk on 26.8.17.
 */
public class DateSerializer {
    private static String dateFormat = "dd-MM-yyyy";
    private static SimpleDateFormat df = new SimpleDateFormat(dateFormat);

    public static Date getDate(String date) {
        Date d = null;
        Calendar calendar = Calendar.getInstance();
        try {
            d = new Date();
            calendar.setTime(df.parse(date));
            d = calendar.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return d;
    }

    public static Date getDateL(String date, @Nullable Integer cal) {
       Date d = null;
        try {
            d = df.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return d;
    }

    public static Calendar getCalenderDate(Date date) {

        Calendar calendar = Calendar.getInstance();

        calendar.setTime(date);

        return calendar;
    }

    public static String getDateStringFromDate(Date date) {
        StringBuilder sDate = new StringBuilder();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        sDate.append(calendar.get(Calendar.DAY_OF_MONTH));
        sDate.append("-");
        sDate.append(calendar.get(Calendar.MONTH));
        sDate.append("-");
        sDate.append(calendar.get(Calendar.YEAR));
        return sDate.toString();
    }

    public static Time generateTime(int hour, int minute, String ap) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR, hour);
        calendar.set(Calendar.MINUTE, minute);
        calendar.set(Calendar.AM_PM, ap.equals("AM") ? Calendar.AM : Calendar.PM);
        return new Time(calendar.getTime().getTime());
    }

    public static Date generateDate(int year, int month, int day) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, day);
        return calendar.getTime();
    }

    public static String getTimeStringFromTime(String time) {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        Calendar calendar = Calendar.getInstance();
        try {
            calendar.setTime(sdf.parse(time));
        } catch (ParseException e) {
            return null;
        }
        String amPm = calendar.get(Calendar.AM_PM) == Calendar.AM ? "AM" : "PM";
        return String.valueOf(String.format("%02d", calendar.get(Calendar.HOUR))) + "-" + String.format("%02d", calendar.get(Calendar.MINUTE)) + " " + amPm;

    }
}
