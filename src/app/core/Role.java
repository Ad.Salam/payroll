package app.core;

/**
 * Created by arifk on 13.11.17.
 */
public enum Role {
    Admin(1), Editor(2), Employee(3), HrManager(4);
    public int val;

    Role(int i) {
        val = i;
    }
}
