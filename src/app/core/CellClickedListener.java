package app.core;

import java.awt.event.MouseEvent;

/**
 * Created by arifk on 8.10.17.
 */
public interface CellClickedListener {
    public void onMouseClicked(int rowNumber, MouseEvent event);
}
