package app.data.dao;

import app.model.User;
import app.model.UserRole;

import java.util.List;

/**
 * Created by arifk on 30.10.17.
 */
public interface UserRoleDAO {
    List<UserRole> getUserRoles();

    UserRole getUserRole(int roleId);
}
