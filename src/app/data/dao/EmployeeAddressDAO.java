package app.data.dao;

import app.model.Address;

import java.util.List;

/**
 * Created by arifk on 25.8.17.
 */
public interface EmployeeAddressDAO {
    boolean addAddress(Address address);

    boolean updateAddress(Address address);

    Address getAddress(int addressId);

    Address getAddressByEmployee(int empId);
    String getCountryNameByCountryId(int cId);
}
