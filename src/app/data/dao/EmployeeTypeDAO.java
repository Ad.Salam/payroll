package app.data.dao;

import app.model.EmployeeType;

import java.util.List;

/**
 * Created by arifk on 25.8.17.
 */
public interface EmployeeTypeDAO {
    boolean updateEmpType(EmployeeType type);
    int addEmpType(EmployeeType type);

    EmployeeType getEmpType(int typeId);

    List<EmployeeType> getEmpTypeList();
}
