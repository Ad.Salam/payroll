package app.data.dao;

import app.model.User;
import app.model.UserRole;

import java.util.List;

/**
 * Created by arifk on 28.10.17.
 */
public interface UserDAO {
    boolean createUser(User user);

    boolean updateUser(User user);

    User getUser(int id);

    List<User> getUserList();

    List<UserRole> getRoles();

    int createRole(UserRole role);

    UserRole getRoleById(int roleId);

    User getUserByEmpId(int empId);

}

