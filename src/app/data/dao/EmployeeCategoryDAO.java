package app.data.dao;

import app.model.EmployeeCategory;

import java.util.List;

/**
 * Created by arifk on 25.8.17.
 */
public interface EmployeeCategoryDAO {
    int addEmpCat(EmployeeCategory category);

    boolean updateEmpCat(EmployeeCategory category);

    EmployeeCategory getCat(int catId);

    List<EmployeeCategory> getCatList();
}
