package app.data.dao;

import app.core.UserSession;
import app.model.Employee;
import app.model.LoginData;
import app.model.User;

/**
 * Created by arifk on 25.8.17.
 */
public interface AuthDAO {
    void createOrUpdateUser(User user);

    User checkLoginInfo(LoginData user);

    UserSession getCurrentSession(int userId);

    int setUserSession(UserSession userSession);

    int updateUserSession(UserSession userSession);

}
