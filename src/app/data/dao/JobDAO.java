package app.data.dao;

import app.model.Job;

import java.util.List;

/**
 * Created by arifk on 25.8.17.
 */
public interface JobDAO {

    int addJob(Job job);

    boolean updateJob(Job job);

    Job getJob(int jobId);

    List<Job> getJobList();

}
