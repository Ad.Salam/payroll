package app.data.dao;

import app.model.TimeSheet;
import com.sun.istack.internal.Nullable;

import java.util.Date;
import java.util.List;

/**
 * Created by arifk on 23.10.17.
 */
public interface AttendanceDAO {
    int addAttendance(TimeSheet timeSheet);

    int updateAttendance(TimeSheet timeSheet);

    TimeSheet getAttendance(Date searchDate);

    TimeSheet getAttendance(int empId);

    List<TimeSheet> searchAttendance(@Nullable Integer empId, @Nullable Integer year, @Nullable Integer month, @Nullable Integer day);

    TimeSheet getAttendance(int empId, Date date);

    List<TimeSheet> getCurrentAttendanceList();

    List<TimeSheet> getAttendanceListByMonth(int year, int month);

    boolean deleteAttendance(int timeSheetId);
}
