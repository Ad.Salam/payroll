package app.data.dao;

import app.model.*;

import java.util.List;

/**
 * Created by arifk on 25.8.17.
 */
public interface EmployeeDAO {
    List<Country> getCountries();

    Employee getEmployee(int empId);

    Employee getEmployee(String empCode);

    List<Employee> getEmployeeList();

    List<Employee> getEmployeeList(int active);

    int addNewEmployee(Employee employee);

    boolean updateEmployee(Employee employee);

}

