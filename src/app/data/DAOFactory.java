package app.data;

import app.config.ConfigKeys;
import app.data.cloud.CloudFactory;
import app.data.dao.*;
import app.data.db.DBFactory;
import app.data.file.FileFactory;

/**
 * Created by arifk on 25.8.17.
 */
public abstract class DAOFactory {


    public abstract AuthDAO getAuthDAO();

    public abstract EmployeeDAO getEmployeeDao();

    public abstract DepartmentDAO getDepartmentDAO();

    public abstract EmployeeAddressDAO getEmployeeAddressDAO();

    public abstract EmployeeCategoryDAO getEmployeeCategoryDAO();

    public abstract EmployeeTypeDAO getEmployeeTypeDAO();

    public abstract JobDAO getJobDAO();

    public abstract ProjectDAO getProjectDAO();

    public abstract AttendanceDAO getAttendanceDAO();

    public abstract UserDAO getUserDAO();

    public abstract UserRoleDAO getUserRoleDAO();

    public abstract SalaryDAO getSalaryDAO();
}
