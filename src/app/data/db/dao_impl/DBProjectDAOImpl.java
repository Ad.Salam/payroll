package app.data.db.dao_impl;

import app.core.DB;
import app.core.ResultParser;
import app.data.dao.ProjectDAO;
import app.model.Project;
import app.model.ProjectDetails;
import com.mysql.jdbc.PreparedStatement;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by arifk on 23.10.17.
 */
public class DBProjectDAOImpl implements ProjectDAO {
    private Connection con;

    public DBProjectDAOImpl() {
        this.con = DB.getConnection();
    }

    @Override
    public int addProject(Project project) {
        PreparedStatement ps = null;
        try {
            String query = "INSERT INTO project (projectId,deptId,managerId,projectTitle,projectDesc,active) VALUES (?,?,?,?,?,?)";
            ps = (PreparedStatement) con.prepareStatement(query);
            ps.setInt(1, project.getProjectId());
            ps.setInt(2, project.getDeptId());
            ps.setInt(3, project.getManagerId());
            ps.setString(4, project.getProjectTitle());
            ps.setString(5, project.getProjectDesc());
            ps.setInt(6, project.getActive());
            ps.executeUpdate();
            int id = 0;
            ResultSet set = ps.getGeneratedKeys();
            while (set.next()) {
                id = (int) set.getLong(1);
            }
            return id;
        } catch (SQLException e) {
            return 0;
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

        }

    }

    @Override
    public int assignProject(ProjectDetails projectDetails) {
        PreparedStatement ps = null;
        try {
            java.sql.Date sqlDate = new java.sql.Date(projectDetails.getJoinDate().getTime());

            String query = "INSERT INTO project_details (projectDetailsId,projectId,empId,joinDate) VALUES (?,?,?,?)";
            ps = (PreparedStatement) con.prepareStatement(query);
            ps.setInt(1, projectDetails.getProjectDetailsId());
            ps.setInt(2, projectDetails.getProjectId());
            ps.setInt(3, projectDetails.getEmpId());
            ps.setDate(4, sqlDate);
            ps.executeUpdate();
            int id = 0;
            ResultSet set = ps.getGeneratedKeys();
            while (set.next()) {
                id = (int) set.getLong(1);
            }
            return id;
        } catch (SQLException e) {
            return 0;
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public int updateProject(Project project) {
        return 0;
    }

    @Override
    public List<Project> getProjectList() {
        PreparedStatement ps = null;
        List<Project> projects = new ArrayList<>();
        try {
            String query = "SELECT * FROM project ";
            ps = (PreparedStatement) con.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            Project project = null;
            while (rs.next()) {
                project = ResultParser.parser(rs, Project.class);
                projects.add(project);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return projects;
    }

    @Override
    public List<ProjectDetails> getAssignedProjectMemberList(int projectId) {
        PreparedStatement ps = null;
        List<ProjectDetails> projects = new ArrayList<>();
        try {
            String query = "SELECT * FROM project_details WHERE projectId =" + projectId;
            ps = (PreparedStatement) con.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            ProjectDetails project = null;
            while (rs.next()) {
                project = ResultParser.parser(rs, ProjectDetails.class);
                projects.add(project);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return projects;

    }

    @Override
    public Project getProject(int pId) {
        PreparedStatement ps = null;
        Project project = null;
        try {
            String query = "SELECT * FROM project WHERE projectId=" + pId;
            ps = (PreparedStatement) con.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                project = ResultParser.parser(rs, Project.class);
            }
        } catch (SQLException e) {
            return null;
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return project;
    }

    @Override
    public boolean deleteProject(int projectId) {
        return false;
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        con.close();
    }
}
