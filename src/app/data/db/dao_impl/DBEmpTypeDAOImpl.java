package app.data.db.dao_impl;

import app.core.DB;
import app.core.ResultParser;
import app.data.dao.EmployeeTypeDAO;
import app.model.EmployeeType;
import com.mysql.jdbc.PreparedStatement;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by arifk on 20.9.17.
 */
public class DBEmpTypeDAOImpl implements EmployeeTypeDAO {
    private Connection con;

    public DBEmpTypeDAOImpl() {
        con = DB.getConnection();
    }

    @Override
    public boolean updateEmpType(EmployeeType type) {
        return false;
    }

    @Override
    public int addEmpType(EmployeeType type) {
        PreparedStatement ps = null;
        try {
            String query = "INSERT INTO emp_type (typeId,typeName,note) VALUES (?,?,?)";
            ps = (PreparedStatement) con.prepareStatement(query);
            ps.setInt(1, type.getTypeId());
            ps.setString(2, type.getName());
            ps.setString(3, type.getNote());
            ps.executeUpdate();
            int id = 0;
            ResultSet set = ps.getGeneratedKeys();
            while (set.next()) {
                id = (int) set.getLong(1);
            }
            return id;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

        }
        return 0;
    }

    @Override
    public EmployeeType getEmpType(int typeId) {
        PreparedStatement ps = null;
        EmployeeType type = null;
        try {
            String query = "SELECT * FROM emp_type WHERE typeId=" + typeId;
            ps = (PreparedStatement) con.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                type = ResultParser.parser(rs, EmployeeType.class);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                ps.close();
                con.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return type;
    }

    @Override
    public List<EmployeeType> getEmpTypeList() {
        PreparedStatement ps = null;
        List<EmployeeType> employeeTypes = new ArrayList<>();
        try {
            String query = "SELECT * FROM emp_type ";
            ps = (PreparedStatement) con.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            EmployeeType employeeType = null;
            while (rs.next()) {
                employeeType = ResultParser.parser(rs, EmployeeType.class);
                employeeTypes.add(employeeType);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

        }
        return employeeTypes;
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        con.close();
    }
}
