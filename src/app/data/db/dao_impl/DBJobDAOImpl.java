package app.data.db.dao_impl;

import app.core.DB;
import app.core.ResultParser;
import app.data.dao.JobDAO;
import app.model.Job;
import com.mysql.jdbc.PreparedStatement;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by arifk on 25.8.17.
 */
public class DBJobDAOImpl implements JobDAO {
    private Connection con;

    public DBJobDAOImpl() {
        con = DB.getConnection();
    }

    @Override
    public int addJob(Job job) {
        PreparedStatement ps = null;
        try {
            String query = "INSERT INTO job(jobId,deptId,jobTitle,jobDesc) VALUES (?,?,?,?)";
            ps = (PreparedStatement) con.prepareStatement(query);
            ps.setInt(1, job.getJobId());
            ps.setInt(2, job.getDeptId());
            ps.setString(3, job.getJobTitle());
            ps.setString(4, job.getJobDesc());
            ps.executeUpdate();
            int id = 0;
            ResultSet set = ps.getGeneratedKeys();
            while (set.next()) {
                id = (int) set.getLong(1);
            }
            return id;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return 0;
    }

    @Override
    public boolean updateJob(Job job) {
        return false;
    }

    @Override
    public Job getJob(int jobId) {
        PreparedStatement ps = null;
        Job job = null;
        try {
            String query = "SELECT * FROM job WHERE jobId=" + jobId;
            ps = (PreparedStatement) con.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                job = ResultParser.parser(rs, Job.class);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {

            try {
                ps.close();
                con.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return job;
    }

    @Override
    public List<Job> getJobList() {
        PreparedStatement ps = null;
        List<Job> jobs = new ArrayList<>();
        try {
            String query = "SELECT * FROM job ";
            ps = (PreparedStatement) con.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            Job job = null;
            while (rs.next()) {
                job = ResultParser.parser(rs, Job.class);
                jobs.add(job);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

        }
        return jobs;
    }


}
