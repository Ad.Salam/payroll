package app.data.db.dao_impl;

import app.core.DB;
import app.core.ResultParser;
import app.data.dao.SalaryDAO;
import app.model.Employee;
import app.model.Payroll;
import app.model.Salary;
import com.mysql.jdbc.PreparedStatement;
import com.sun.istack.internal.Nullable;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by arifk on 12.12.17.
 */
public class DBSalaryDAOImpl implements SalaryDAO {
    private Connection connection;

    public DBSalaryDAOImpl() {
        connection = DB.getConnection();
    }

    @Override
    public List<Salary> getSalaryList() {
        PreparedStatement ps = null;
        List<Salary> salaries = new ArrayList<>();
        try {
            String query = "SELECT * FROM salary";
            ps = (PreparedStatement) connection.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Salary salary = ResultParser.parser(rs, Salary.class);
                salaries.add(salary);
            }

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return salaries;
    }

    @Override
    public List<Salary> getSalaryList(int empId) {
        PreparedStatement ps = null;
        List<Salary> salaries = new ArrayList<>();
        try {
            String query = "SELECT * FROM salary WHERE empId=" + empId;


            ps = (PreparedStatement) connection.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Salary salary = ResultParser.parser(rs, Salary.class);
                salaries.add(salary);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

        }
        return salaries;
    }

    @Override
    public Salary getSalaryByEmpId(int empId, int year) {
        PreparedStatement ps = null;
        Salary salary = null;
        try {
            String query = "SELECT * FROM salary WHERE empId =" + empId + " AND year=" + year;
            ps = (PreparedStatement) connection.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                salary = ResultParser.parser(rs, Salary.class);
            }

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

        }
        return salary;

    }

    @Override
    public Salary getSalaryBySalaryId(int salId) {
        PreparedStatement ps = null;
        Salary salary = null;
        try {
            String query = "SELECT * FROM salary WHERE salaryId =" + salId;
            ps = (PreparedStatement) connection.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                salary = ResultParser.parser(rs, Salary.class);
            }

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

        }
        return salary;
    }

    @Override
    public int insertSalary(Salary salary) {
        PreparedStatement ps = null;
        try {
            String query = "INSERT INTO salary (salaryId,empId,year,note,amount) VALUES (?,?,?,?,?)";
            ps = (PreparedStatement) connection.prepareStatement(query);
            ps.setInt(1, salary.getSalaryId());
            ps.setInt(2, salary.getEmpId());
            ps.setInt(3, salary.getYear());
            ps.setString(4, salary.getNote());
            ps.setDouble(5, salary.getAmount());
            ps.executeUpdate();
            int id = 0;
            ResultSet set = ps.getGeneratedKeys();

            while (set.next()) {
                id = (int) set.getLong(1);
            }
            return id;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

        }
        return 0;
    }

    @Override
    public Payroll getPayroll(int payrollId) {
        PreparedStatement ps = null;
        Payroll payroll = null;
        try {
            String query = "SELECT * FROM payroll WHERE payrollId =" + payrollId;
            ps = (PreparedStatement) connection.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                payroll = ResultParser.parser(rs, Payroll.class);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

        }
        return payroll;
    }

    @Override
    public Payroll getPayroll(int empId, int year, int month) {
        PreparedStatement ps = null;
        Payroll payroll = null;
        try {
            String query = "SELECT * FROM payroll WHERE empId =" + empId + " AND YEAR (date)=" + year + " AND MONTH (date) =" + month;
            ps = (PreparedStatement) connection.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                payroll = ResultParser.parser(rs, Payroll.class);
            }

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

        }
        return payroll;
    }

    @Override
    public List<Payroll> getPayrollList() {
        PreparedStatement ps = null;
        List<Payroll> payrollList = new ArrayList<>();
        try {
            String query = "SELECT * FROM payroll";
            ps = (PreparedStatement) connection.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Payroll payroll = ResultParser.parser(rs, Payroll.class);
                payrollList.add(payroll);
            }

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

        }
        return payrollList;
    }

    @Override
    public List<Payroll> getPayrollList(@Nullable Integer empId, @Nullable Integer year, @Nullable Integer month) {
        PreparedStatement ps = null;
        List<Payroll> payrollList = new ArrayList<>();
        try {
            String query = "SELECT * FROM payroll WHERE ";

            if (empId != null) {

                    query = query.concat("empId=" + empId);

            }
            if (year != null) {
                if (empId != null) {
                    query = query.concat(" AND YEAR (date) =" + year);
                } else
                    query = query.concat(" YEAR (date)=" + year);

            }
            if (month != null) {
                if (empId != null || year!=null) {
                    query = query.concat(" AND MONTH (date)=" + month);
                } else {
                    query = query.concat(" MONTH (date)=" + month);
                }
            }

            ps = (PreparedStatement) connection.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Payroll payroll = ResultParser.parser(rs, Payroll.class);
                payrollList.add(payroll);
            }

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return payrollList;
    }

    @Override
    public int insertPayroll(Payroll payroll) {
        java.sql.Date sqlDate = new java.sql.Date(payroll.getDate().getTime());
        PreparedStatement ps = null;
        try {
            String query = "INSERT INTO payroll (payrollId,empId,date,hoursWorked,grossPay,deductions,netPay) VALUES (?,?,?,?,?,?,?)";
            ps = (PreparedStatement) connection.prepareStatement(query);
            ps.setInt(1, payroll.getPayrollId());
            ps.setInt(2, payroll.getEmpId());
            ps.setDate(3, sqlDate);
            ps.setInt(4, payroll.getHoursWorked());
            ps.setDouble(5, payroll.getGrossPay());
            ps.setDouble(6, payroll.getDeductions());
            ps.setDouble(7, payroll.getNetPay());
            ps.executeUpdate();
            int id = 0;
            ResultSet set = ps.getGeneratedKeys();
            while (set.next()) {
                id = (int) set.getLong(1);
            }
            return id;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

        }
        return 0;
    }

    @Override
    public int updateSalary(Salary salary) {
        PreparedStatement ps = null;
        try {
            String query = "UPDATE salary SET salaryId=?,empId=?,year=?,note=?,amount=? WHERE salaryId=" + salary.getSalaryId();
            ps = (PreparedStatement) connection.prepareStatement(query);
            ps.setInt(1, salary.getSalaryId());
            ps.setInt(2, salary.getEmpId());
            ps.setInt(3, salary.getYear());
            ps.setString(4, salary.getNote());
            ps.setDouble(5, salary.getAmount());
            ps.executeUpdate();

            return ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

        }
        return 0;
    }

    @Override
    public int updatePayroll(Payroll payroll) {
        java.sql.Date sqlDate = new java.sql.Date(payroll.getDate().getTime());
        PreparedStatement ps = null;
        try {
            String query = "UPDATE payroll set payrollId=?,empId=?,date=?,hoursWorked=?,grossPay=?,deductions=?,netPay=? WHERE payrollId=" + payroll.getPayrollId();
            ps = (PreparedStatement) connection.prepareStatement(query);
            ps.setInt(1, payroll.getPayrollId());
            ps.setInt(2, payroll.getEmpId());
            ps.setDate(3, sqlDate);
            ps.setInt(4, payroll.getHoursWorked());
            ps.setDouble(5, payroll.getGrossPay());
            ps.setDouble(6, payroll.getDeductions());
            ps.setDouble(7, payroll.getNetPay());
            return ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

        }
        return 0;
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        connection.close();
    }
}
