package app.data.db.dao_impl;

import app.core.DB;
import app.core.ResultParser;
import app.data.dao.UserRoleDAO;
import app.model.User;
import app.model.UserRole;
import com.mysql.jdbc.PreparedStatement;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by arifk on 30.10.17.
 */
public class DBRoleDAOImpl implements UserRoleDAO {

    Connection connection;

    public DBRoleDAOImpl() {
        connection = DB.getConnection();
    }


    @Override
    public List<UserRole> getUserRoles() {
        PreparedStatement ps = null;
        String query = "SELECT * From role";
        List<UserRole> roles = new ArrayList<>();
        try {
            ps = (PreparedStatement) connection.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                UserRole userRole = ResultParser.parser(rs, UserRole.class);
                roles.add(userRole);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            try {
                if (ps != null)
                    ps.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return roles;
    }

    @Override
    public UserRole getUserRole(int roleId) {
        PreparedStatement ps = null;
        String query = "SELECT * From role WHERE roleId=" + roleId;
        UserRole role = null;
        try {
            ps = (PreparedStatement) connection.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                role = ResultParser.parser(rs, UserRole.class);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            try {
                if (ps != null)
                    ps.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return role;
    }

}
