package app.data.db.dao_impl;

import app.core.DB;
import app.core.Encryption;
import app.core.ResultParser;
import app.data.dao.UserDAO;
import app.model.User;
import app.model.UserRole;
import com.mysql.jdbc.PreparedStatement;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by arifk on 28.10.17.
 */
public class DBUserDAOImpl implements UserDAO {
    private Connection connection;

    public DBUserDAOImpl() {
        this.connection = DB.getConnection();
    }

    @Override
    public boolean createUser(User user) {
        PreparedStatement ps = null;
        int id = 0;
        try {
            String query = "INSERT INTO users (userId,roleId,empId,password,active) VALUES (?,?,?,?,?)";
            ps = (PreparedStatement) connection.prepareStatement(query);
            ps.setInt(1, user.getUserId());
            ps.setInt(2, user.getRoleId());
            ps.setInt(3, user.getEmpId());
            ps.setString(4, Encryption.getStringToMd5Value(user.getPassword()));
            ps.setInt(5, user.isActive() ? 1 : 0);
            ps.executeUpdate();

            ResultSet set = ps.getGeneratedKeys();
            while (set.next()) {
                id = (int) set.getLong(1);
            }
            return id != 0;
        } catch (SQLException e) {
            return id <= 0;
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

        }
    }

    @Override
    public boolean updateUser(User user) {
        PreparedStatement ps = null;
        int id = 0;
        try {
            String query = "UPDATE users SET userId=?,roleId=?,empId=?,active=? WHERE userId=" + user.getUserId();
            ps = (PreparedStatement) connection.prepareStatement(query);
            ps.setInt(1, user.getUserId());
            ps.setInt(2, user.getRoleId());
            ps.setInt(3, user.getEmpId());
            ps.setInt(4, user.isActive() ? 1 : 0);
            ps.executeUpdate();
            ResultSet set = ps.getGeneratedKeys();
            while (set.next()) {
                id = (int) set.getLong(1);
            }
            return id != 0;
        } catch (SQLException e) {
            return id <= 0;
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

        }
    }

    @Override
    public User getUser(int id) {
        PreparedStatement ps = null;
        String query = "SELECT * From users";
        User user = new User();
        try {
            ps = (PreparedStatement) connection.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                user = ResultParser.parser(rs, User.class);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            try {
                if (ps != null)
                    ps.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return user;
    }

    @Override
    public List<User> getUserList() {
        PreparedStatement ps = null;
        String query = "SELECT * From users";
        List<User> users = new ArrayList<>();
        try {
            ps = (PreparedStatement) connection.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            User user;
            while (rs.next()) {
                user = ResultParser.parser(rs, User.class);
                users.add(user);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            try {
                if (ps != null)
                    ps.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return users;
    }

    @Override
    public List<UserRole> getRoles() {
        PreparedStatement ps = null;
        String query = "SELECT * From role";
        List<UserRole> roles = new ArrayList<>();
        try {
            ps = (PreparedStatement) connection.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            UserRole role;
            while (rs.next()) {
                role = ResultParser.parser(rs, UserRole.class);
                roles.add(role);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            try {
                if (ps != null)
                    ps.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return roles;
    }

    @Override
    public int createRole(UserRole role) {
        return 0;
    }

    @Override
    public UserRole getRoleById(int roleId) {
        PreparedStatement ps = null;
        String query = "SELECT * From role WHERE roleId=" + roleId;
        UserRole role = new UserRole();
        try {
            ps = (PreparedStatement) connection.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                role = ResultParser.parser(rs, UserRole.class);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            try {
                if (ps != null)
                    ps.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return role;
    }

    @Override
    public User getUserByEmpId(int empId) {
        PreparedStatement ps = null;
        String query = "SELECT * From users WHERE empId=" + empId;
        User user = null;
        try {
            ps = (PreparedStatement) connection.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                user = ResultParser.parser(rs, User.class);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            try {
                if (ps != null)
                    ps.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return user;
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        connection.close();
    }
}
