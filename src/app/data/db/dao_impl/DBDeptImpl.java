package app.data.db.dao_impl;

import app.core.DB;
import app.core.ResultParser;
import app.data.dao.DepartmentDAO;
import app.model.Department;
import com.mysql.jdbc.PreparedStatement;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by arifk on 20.9.17.
 */
public class DBDeptImpl implements DepartmentDAO {
    private Connection con;

    public DBDeptImpl() {
        con = DB.getConnection();
    }

    @Override
    public int addDepartment(Department department) {
        PreparedStatement ps = null;
        try {
            String query = "INSERT INTO department (deptId,deptName,deptDesc,deptParentId,managerId,location) VALUES (?,?,?,?,?,?)";
            ps = (PreparedStatement) con.prepareStatement(query);
            ps.setInt(1, department.getDeptId());
            ps.setString(2, department.getDeptName());
            ps.setString(3, department.getDeptDesc());
            ps.setInt(4, department.getDeptParentId());
            ps.setInt(5, department.getManagerId());
            ps.setString(6, department.getLocation());
            ps.executeUpdate();
            int id = 0;
            ResultSet set = ps.getGeneratedKeys();
            while (set.next()) {
                id = (int) set.getLong(1);
            }
            return id;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

        }
        return 0;
    }

    @Override
    public int updateDepartment(Department department) {
        return 0;
    }

    @Override
    public Department getDepartment(int deptId) {
        PreparedStatement ps = null;
        Department department = null;
        try {
            String query = "SELECT * FROM department WHERE deptId=" + deptId;
            ps = (PreparedStatement) con.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                department = ResultParser.parser(rs, Department.class);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                ps.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return department;
    }

    @Override
    public List<Department> getDepartmentList() {
        PreparedStatement ps = null;
        List<Department> departments = new ArrayList<>();
        try {
            String query = "SELECT * FROM department ";
            ps = (PreparedStatement) con.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            Department department = null;
            while (rs.next()) {
                department = ResultParser.parser(rs, Department.class);
                departments.add(department);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

        }
        return departments;
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        con.close();
    }
}
