package app.config;

import app.data.DAOFactory;
import app.data.cloud.CloudFactory;
import app.data.db.DBFactory;
import app.data.file.FileFactory;

/**
 * Created by arifk on 26.8.17.
 */
public class DataSource {
    private static int DATA_SOURCE = ConfigKeys.DB;

    public static DAOFactory getDataSource() {
        switch (DATA_SOURCE) {
            case ConfigKeys.DB:
                return new DBFactory();
            case ConfigKeys.CLOUD:
                return new CloudFactory();
            case ConfigKeys.FILE:
                return new FileFactory();
            default:
                return new DBFactory();
        }
    }


}
