package app.modules.Project;

import app.model.Department;
import app.model.Employee;
import app.model.Project;
import app.model.ProjectDetails;
import app.modules.Common.BaseView;

import java.util.List;


/**
 * Created by arifk on 6.11.17.
 */
public interface ProjectContract {
    interface ProjectPresenter {
        int addProject(Project project);

        void getManagerList();

        void getDepartmentList();

        void getProjectList();

        int assignProject(ProjectDetails projectDetails);

        void getAssignedMembersList(int projectId);
    }

    interface ProjectView extends BaseView {
        void setMangerListToView(List<Employee> employees);

        void setDepartmentListToView(List<Department> departments);

        void setProjectListToView(List<Project> projectList);

        void setAssignedMembersToView(List<ProjectDetails> details);
    }
}
