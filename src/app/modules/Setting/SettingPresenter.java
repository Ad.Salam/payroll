package app.modules.Setting;

import app.config.DataSource;
import app.data.DAOFactory;
import app.model.*;

import java.util.List;

/**
 * Created by arifk on 20.9.17.
 */
public class SettingPresenter implements SettingContract.AdminPresenter {

    private DAOFactory daoFactory = DataSource.getDataSource();
    private SettingContract.SettingView view;

    public SettingPresenter(SettingContract.SettingView view) {
        this.view = view;
    }

    @Override
    public void getDepartments() {
        List<Department> departments = daoFactory.getDepartmentDAO().getDepartmentList();
        for (Department dept : departments) {
            dept.setManager(daoFactory.getEmployeeDao().getEmployee(dept.getManagerId()));
            dept.setParentDept(daoFactory.getDepartmentDAO().getDepartment(dept.getDeptParentId()));
        }
        view.setDepartmentDataToView(departments);
    }

    @Override
    public void getJobs() {
        List<Job> jobs = daoFactory.getJobDAO().getJobList();
        for (Job job : jobs) {
            job.setDepartment(daoFactory.getDepartmentDAO().getDepartment(job.getDeptId()));
        }
        view.setJobsData(jobs);
    }

    @Override
    public void getEmployeeList() {
        view.setEmployeeListToCombo(daoFactory.getEmployeeDao().getEmployeeList(1));
    }

    @Override
    public void getEmployeeTypes() {
        view.setTypes(daoFactory.getEmployeeTypeDAO().getEmpTypeList());
    }

    @Override
    public void getEmployeeCategories() {
        view.setCategoriesData(daoFactory.getEmployeeCategoryDAO().getCatList());
    }

    @Override
    public void getUserList() {
        List<User> users = daoFactory.getUserDAO().getUserList();
        for (User user : users) {
            user.setEmployee(daoFactory.getEmployeeDao().getEmployee(user.getEmpId()));
            user.setRole(daoFactory.getUserDAO().getRoleById(user.getRoleId()));
        }
        view.setUserList(users);
    }

    @Override
    public boolean createUser(User user) {
        return daoFactory.getUserDAO().createUser(user);
    }

    @Override
    public int createType(EmployeeType type) {
        return daoFactory.getEmployeeTypeDAO().addEmpType(type);
    }

    @Override
    public int createDepartment(Department department) {
        return daoFactory.getDepartmentDAO().addDepartment(department);
    }

    @Override
    public int createCategory(EmployeeCategory category) {
       return daoFactory.getEmployeeCategoryDAO().addEmpCat(category);
    }

    @Override
    public int createJob(Job job) {
        return daoFactory.getJobDAO().addJob(job);
    }

    @Override
    public User getUser(int userId) {
        return daoFactory.getUserDAO().getUser(userId);
    }

    @Override
    public Employee checkEmpId(String empCode) {
        return daoFactory.getEmployeeDao().getEmployee(empCode);
    }

    @Override
    public boolean checkUserExits(int empId) {
        return daoFactory.getUserDAO().getUserByEmpId(empId) != null;
    }

    @Override
    public void getUserRoles() {
        view.setUserRolesToView(daoFactory.getUserRoleDAO().getUserRoles());
    }
}
