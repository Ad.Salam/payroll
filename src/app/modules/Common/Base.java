package app.modules.Common;

import app.core.DB;
import app.core.UserSession;
import app.model.User;
import app.modules.Auth.AuthContract;
import app.modules.Auth.AuthPresenter;
import app.ui.MainMenu;
import com.sun.java.swing.plaf.windows.WindowsClassicLookAndFeel;
import com.sun.java.swing.plaf.windows.WindowsLookAndFeel;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.plaf.nimbus.NimbusLookAndFeel;
import java.awt.*;
import java.io.File;
import java.util.Date;

/**
 * Created by arifk on 22.8.17.
 */
public abstract class Base extends JFrame {
    public static User currentUser;

    public static boolean isAuth() {
        return currentUser != null;
    }

    public void message(String msg) {
        JOptionPane.showMessageDialog(null, msg);
    }

    public void setUi(JPanel panel) {
        setDefaultLookAndFeelDecorated(true);
        try {
            UIManager.setLookAndFeel(new WindowsLookAndFeel());
        } catch (UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }

        setLocationRelativeTo(null);
        setJMenuBar(new MainMenu().getMenu(this));
        setContentPane(panel);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        try {
            Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
            setSize(screenSize.width, screenSize.height);
            setSize(700, 500);
            setExtendedState(Frame.MAXIMIZED_BOTH);
            setVisible(true);
            setIconImage(ImageIO.read(ClassLoader.getSystemResource("res/logo.png")));
        } catch (Exception e) {
            message(e.getMessage());
        }
    }


    public boolean isDbConnected() {
        if (DB.getConnection() == null) {
            message("Database Not Connected");
            return false;
        } else {
            return true;
        }
    }

    private void removeUserSession() {

    }

    private void checkUserSession() {
        //   UserSession userSession = presenter.getCurrentUserSession();
    }

    public void insertUserToSession(User user) {
        AuthContract.SessionPresenter presenter = new AuthPresenter(null);
        UserSession userSession = new UserSession();
        userSession.setUserId(user.getUserId());
        userSession.setStartLogin(new Date());
        userSession.setAuth(true);
        presenter.setCurrentSession(userSession);
    }
}
