package app.modules.Common;

/**
 * Created by arifk on 22.8.17.
 */
public interface BaseView {
    void setUi();
    void init();

}
