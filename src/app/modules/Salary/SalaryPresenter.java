package app.modules.Salary;

import app.config.DataSource;
import app.data.DAOFactory;
import app.data.db.DBFactory;
import app.model.Employee;
import app.model.Payroll;
import app.model.Salary;
import com.sun.istack.internal.Nullable;

import java.util.List;

/**
 * Created by arifk on 21.8.17.
 */
public class SalaryPresenter implements SalaryContract.SalaryPresenter {
    private SalaryContract.salaryView view;
    private DAOFactory daoFactory;

    public SalaryPresenter(SalaryContract.salaryView view) {
        this.view = view;
        daoFactory = DataSource.getDataSource();
    }

    @Override
    public int insertSalary(Salary salary) {
        return daoFactory.getSalaryDAO().insertSalary(salary);
    }

    @Override
    public int updateSalary(Salary salary) {
        return daoFactory.getSalaryDAO().updateSalary(salary);
    }

    @Override
    public int insertPayroll(Payroll payroll) {
        return daoFactory.getSalaryDAO().insertPayroll(payroll);
    }

    @Override
    public int updatePayroll(Payroll payroll) {
        return daoFactory.getSalaryDAO().updatePayroll(payroll);
    }

    @Override
    public void getActiveEmployee() {
        List<Employee> employees = daoFactory.getEmployeeDao().getEmployeeList(1);
        view.setEmployeeToView(employees);
    }

    @Override
    public Salary getSalaryByEmpId(int empId, int year) {
        Salary salary = daoFactory.getSalaryDAO().getSalaryByEmpId(empId, year);
        if (salary != null) {
            Employee employee = daoFactory.getEmployeeDao().getEmployee(salary.getEmpId());
            salary.setEmployee(employee);
        }

        return salary;
    }

    @Override
    public void getSalaryList() {
        List<Salary> salaries = daoFactory.getSalaryDAO().getSalaryList();
        for (Salary salary : salaries) {
            Employee employee = daoFactory.getEmployeeDao().getEmployee(salary.getEmpId());
            salary.setEmployee(employee);
        }
        view.setSalariesToView(salaries);
    }

    @Override
    public void getSalaryList(int empId) {
        List<Salary> salaries = daoFactory.getSalaryDAO().getSalaryList(empId);
        for (Salary salary : salaries) {
            Employee employee = daoFactory.getEmployeeDao().getEmployee(salary.getEmpId());
            salary.setEmployee(employee);
        }
        view.setSalariesToView(salaries);
    }

    @Override
    public void getPayList() {
        List<Payroll> payrollList = daoFactory.getSalaryDAO().getPayrollList();
        for (Payroll payroll : payrollList) {
            Employee employee = daoFactory.getEmployeeDao().getEmployee(payroll.getEmpId());
            payroll.setEmployee(employee);
        }
        view.setPayListToView(payrollList);
    }

    @Override
    public void getPayList(@Nullable Integer empId, @Nullable Integer year, @Nullable Integer month) {
        List<Payroll> payrollList = daoFactory.getSalaryDAO().getPayrollList(empId, year, month==0?null:month);
        for (Payroll payroll : payrollList) {
            Employee employee = daoFactory.getEmployeeDao().getEmployee(payroll.getEmpId());
            payroll.setEmployee(employee);
        }
        view.setPayListToView(payrollList);
    }

    @Override
    public Payroll getPayrollByEmpId(int empId, int year, int month) {
        return daoFactory.getSalaryDAO().getPayroll(empId, year, month);
    }
}
