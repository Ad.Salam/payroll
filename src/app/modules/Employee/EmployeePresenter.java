package app.modules.Employee;

import app.config.DataSource;
import app.data.DAOFactory;
import app.model.Employee;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * Created by arifk on 21.8.17.
 */
public class EmployeePresenter implements EmployeeContract.EmployeePresenter {
    private EmployeeContract.EmployeeView view;
    private DAOFactory daoFactory;

    public EmployeePresenter(EmployeeContract.EmployeeView view) {
        this.view = view;
        daoFactory=DataSource.getDataSource();
    }


    @Override
    public void getEmployeeData() {
        List<Employee> employees = daoFactory.getEmployeeDao().getEmployeeList();
        populateEmployeeList(employees);
        view.setEmployeeData(employees);
    }

    @Override
    public List<Employee> getEmpByJobId(int id) {
        List<Employee> employees = daoFactory.getEmployeeDao().getEmployeeList();
        populateEmployeeList(employees);
        employees.stream().filter(employee -> employee.getJobId() == id).collect(Collectors.toList());
        return employees;
    }

    private void populateEmployeeList(List<Employee> employees) {
        for (Employee employee : employees) {
            employee.setAddress(daoFactory.getEmployeeAddressDAO().getAddressByEmployee(employee.getEmpId()));
            employee.setJob(employee.getJobId() != 0 ? daoFactory.getJobDAO().getJob(employee.getJobId()) : null);
            employee.setCategory(employee.getCatId() != 0 ? daoFactory.getEmployeeCategoryDAO().getCat(employee.getCatId()) : null);
            employee.setType(employee.getTypeId() != 0 ? daoFactory.getEmployeeTypeDAO().getEmpType(employee.getTypeId()) : null);
            employee.setDepartment(employee.getDepartmentId() != 0 ? daoFactory.getDepartmentDAO().getDepartment(employee.getDepartmentId()) : null);
        }
    }

    @Override
    public List<Employee> getEmpByDeptId(int Id) {
        return null;
    }

    @Override
    public List<Employee> getEmpByCatId(int Id) {
        return null;
    }

    @Override
    public List<Employee> getEmpByTypeId(int Id) {
        return null;
    }

    @Override
    public void getEmployeeDetails(int empId) {
        Employee employee = daoFactory.getEmployeeDao().getEmployee(empId);
        employee.setAddress(daoFactory.getEmployeeAddressDAO().getAddressByEmployee(employee.getEmpId()));
        employee.setJob(employee.getJobId() != 0 ? daoFactory.getJobDAO().getJob(employee.getJobId()) : null);
        employee.setCategory(employee.getCatId() != 0 ? daoFactory.getEmployeeCategoryDAO().getCat(employee.getCatId()) : null);
        employee.setType(employee.getTypeId() != 0 ? daoFactory.getEmployeeTypeDAO().getEmpType(employee.getTypeId()) : null);
        employee.setDepartment(employee.getDepartmentId() != 0 ? daoFactory.getDepartmentDAO().getDepartment(employee.getDepartmentId()) : null);
        view.setEmployeeDetails(employee);
    }

    @Override
    public String getCountryName(int cId) {
        return daoFactory.getEmployeeAddressDAO().getCountryNameByCountryId(cId);
    }
}
