package app.modules.Employee;

import app.config.DataSource;
import app.data.DAOFactory;
import app.model.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by arifk on 14.9.17.
 */
public class AddEmployeePresenter implements EmployeeContract.AddEmployeePresenter {
    private EmployeeContract.AddEmployeeView addEmployeeView;
    private DAOFactory daoFactory = DataSource.getDataSource();

    public AddEmployeePresenter(EmployeeContract.AddEmployeeView addEmployeeView) {
        this.addEmployeeView = addEmployeeView;
    }


    @Override
    public Employee getEmployee(int id) {
        Employee employee = daoFactory.getEmployeeDao().getEmployee(id);
        employee.setAddress(daoFactory.getEmployeeAddressDAO().getAddressByEmployee(employee.getEmpId()));
        employee.setCategory(daoFactory.getEmployeeCategoryDAO().getCat(employee.getCatId()));
        employee.setType(daoFactory.getEmployeeTypeDAO().getEmpType(employee.getTypeId()));
        employee.setDepartment(daoFactory.getDepartmentDAO().getDepartment(employee.getDepartmentId()));
        employee.setJob(daoFactory.getJobDAO().getJob(employee.getJobId()));
        return employee;
    }

    @Override
    public List<Department> getDepartment() {
        return daoFactory.getDepartmentDAO().getDepartmentList();
    }

    @Override
    public List<EmployeeCategory> getCategory() {
        return daoFactory.getEmployeeCategoryDAO().getCatList();
    }

    @Override
    public List<EmployeeType> getType() {
        return daoFactory.getEmployeeTypeDAO().getEmpTypeList();
    }

    @Override
    public List<Country> getCountry() {
        return daoFactory.getEmployeeDao().getCountries();
    }

    @Override
    public List<Job> getJobByDeptId(int deptId) {
        List<Job> jobs = daoFactory.getJobDAO().getJobList();
        List<Job> jobsByDept = new ArrayList<>();
        if (jobs == null)
            return jobsByDept;
        else {
            jobs.forEach(job -> {
                if (job.getDeptId() == deptId) {
                    jobsByDept.add(job);
                }
            });

        }
        return jobsByDept;
    }

    @Override
    public boolean addNewEmployee(Employee employee) {
        if (employee.getEmpId() == 0) {
            int id = daoFactory.getEmployeeDao().addNewEmployee(employee);
            employee.getAddress().setEmp_id(id);
            daoFactory.getEmployeeAddressDAO().addAddress(employee.getAddress());
            return id != 0;
        } else {
            daoFactory.getEmployeeAddressDAO().updateAddress(employee.getAddress());
            return daoFactory.getEmployeeDao().updateEmployee(employee);
        }
    }
}
