package app.modules.Attendence;

import app.model.Employee;
import app.model.Project;
import app.model.TimeSheet;
import app.modules.Common.BaseView;
import com.sun.istack.internal.Nullable;

import java.sql.Time;
import java.util.Date;
import java.util.List;

/**
 * Created by arifk on 22.10.17.
 */
public interface AttendanceContract {
    interface Presenter {
        void addAttendance(TimeSheet timeSheet);

        void getEmployees();

        TimeSheet getAttendance(int empId, Date date);

        void searchAttendanceReport(@Nullable Integer empId, @Nullable Integer year, @Nullable Integer month, @Nullable Integer day);

        void getProjects();

        void getLastAttendanceList();

        void getAttendanceListByMonth(int year, int month);
    }

    interface View extends BaseView {
        void setEmployeeListToView(List<Employee> employeeList);

        void setProjectsToView(List<Project> projects);

        void success(Object obj);

        void error(Object error);

        void setAttendanceListToView(List<TimeSheet> timeSheets);

        void setAttendanceByMonthListToView(List<Employee> timeSheets);

        void setAttendanceReportToView(List<TimeSheet> timeSheets);
    }
}