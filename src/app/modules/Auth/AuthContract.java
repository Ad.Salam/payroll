package app.modules.Auth;

import app.core.UserSession;
import app.model.LoginData;
import app.model.User;
import app.modules.Common.BaseView;
import com.sun.istack.internal.Nullable;

/**
 * Created by arifk on 22.8.17.
 */
public interface AuthContract {

    interface AuthView extends BaseView {

        void isAuth(boolean auth, @Nullable User user);
    }

    interface AuthPresenter {
        void checkLoginInfo(LoginData login);


    }

    interface SessionPresenter {

        int setCurrentSession(UserSession session);

        UserSession getCurrentUserSession(int userId);

        boolean updateCurrentSession(UserSession session);

        boolean checkLogin();
    }
}
