create table bonus
(
	bouns_id int auto_increment,
	emp_id int not null,
	date_bonus date not null,
	amount int not null,
	note text not null
)
;

create table countries
(
	id int auto_increment,
	sortname varchar(3) not null,
	con_name varchar(150) not null,
	phonecode int not null
)
;

create table deduction
(
	deduction_id int auto_increment,
	emp_id int(10) not null,
	deduction_type tinyint(1) default '0' not null,
	amount double not null,
	note text not null,
	created_date timestamp default CURRENT_TIMESTAMP not null
)
;

create table department
(
	deptId int auto_increment,
	managerId int(10) not null,
	deptParentId int(10) not null,
	deptName varchar(40) not null,
	location varchar(30) not null,
	deptDesc text not null
)
;

create table emp_address
(
	address?d int(10) auto_increment,
	address1 varchar(100) not null,
	address2 varchar(100) not null,
	city int(25) not null,
	state varchar(20) not null,
	zip int(6) not null,
	countryId int(10) not null,
	empId int(10) not null,
	createDate timestamp default CURRENT_TIMESTAMP not null
)
;

create table emp_category
(
	catId int auto_increment,
	catName varchar(30) not null,
	catDesc varchar(100) not null,
	note text not null
)
;

create table emp_type
(
	typeId int auto_increment,
	typeName int not null,
	note int not null
)
;

create table employees
(
	empId int auto_increment,
	deptId int(10) not null,
	jobId int(10) not null,
	catId int(10) not null,
	typeId int(10) not null,
	firstName varchar(25) not null,
	lastName varchar(20) not null,
	dob date not null,
	email varchar(50) not null,
	cellPhone varchar(17) not null,
	officePhone varchar(17) not null,
	gender tinyint(1) default '5' not null,
	active tinyint(1) default '0' not null,
	ssn varchar(20) not null,
	empCode varchar(12) not null,
	lastLogin date not null,
	dateLignup date not null,
	createDate timestamp default CURRENT_TIMESTAMP not null
)
comment 'employee table'
;

create table event
(
	event_id int auto_increment,
	dept_id int not null,
	event_date int not null,
	event_type int not null,
	event_desc int not null,
	posted_by varchar(30) not null,
	date_posted date not null,
	expiry_date date not null,
	active tinyint(1) default '0' not null
)
;

create table holidays
(
	hoiday_id int auto_increment,
	emp_id int(10) not null,
	date_holiday date not null
)
;

create table hourly
(
	hour_id int auto_increment,
	hourly_rate int(10) not null,
	emp_id int(10) not null,
	note text not null
)
;

create table increment
(
	increment_id int auto_increment,
	amount double not null,
	emp_id int(10) not null,
	createDate timestamp default CURRENT_TIMESTAMP not null
)
;

create table job
(
	jobId int auto_increment,
	deptId int not null,
	jobTitle varchar(100) not null,
	jobDesc text not null
)
;

create table locks
(
	lock_id int auto_increment,
	emp_id int(10) not null,
	lock_date date not null,
	reason_lock text not null,
	active tinyint(1) default '0' not null
)
;

create table messages
(
	message_id int auto_increment,
	emp_id int(10) not null,
	message text not null,
	date_posted int not null,
	posted_by varchar(30) not null,
	num_views int(10) not null,
	active tinyint(1) default '0' not null
)
;

create table payroll
(
	payroll_id int auto_increment,
	emp_id int(10) not null,
	date date not null,
	start_date date not null,
	end_date date not null,
	hours_worked int(6) not null,
	gross_pay double not null,
	deductions double not null,
	net_pay double not null
)
;

create table project
(
	project_id int auto_increment,
	dept_id int(10) not null,
	project_title varchar(100) not null,
	project_desc text not null,
	hours_worked int(10) not null,
	active tinyint(1) default '0' not null
)
;

create table role
(
	roleId int auto_increment,
	roleName varchar(25) not null
)
;

create table salary
(
	salary_id int auto_increment,
	emp_id int(10) not null,
	year int(5) not null,
	note int not null
)
;

create table sickdays
(
	sickday_id int auto_increment,
	emp_id int(10) not null,
	date_sick date not null
)
;

create table timesheet
(
	timesheet_id int auto_increment,
	emp_id int(10) not null,
	project_id int(10) not null,
	check_in time not null,
	check_out time not null,
	work_desc text not null,
	checked tinyint(1) default '0' not null
)
;

create table users
(
	user_id int auto_increment,
	role_id int(10) not null,
	emp_id int(10) not null,
	password varchar(50) not null,
	active tinyint(1) default '0' not null
)
;

