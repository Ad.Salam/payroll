-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 12, 2017 at 09:53 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hr_payroll`
--

-- --------------------------------------------------------

--
-- Table structure for table `bonus`
--

CREATE TABLE `bonus` (
  `bounsId` int(11) NOT NULL,
  `empId` int(11) NOT NULL,
  `dateBonus` date NOT NULL,
  `amount` int(11) NOT NULL,
  `note` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(11) NOT NULL,
  `sortname` varchar(3) NOT NULL,
  `con_name` varchar(150) NOT NULL,
  `phonecode` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `sortname`, `con_name`, `phonecode`) VALUES
(1, 'AF', 'Afghanistan', 93),
(2, 'AL', 'Albania', 355),
(3, 'DZ', 'Algeria', 213),
(4, 'AS', 'American Samoa', 1684),
(5, 'AD', 'Andorra', 376),
(6, 'AO', 'Angola', 244),
(7, 'AI', 'Anguilla', 1264),
(8, 'AQ', 'Antarctica', 0),
(9, 'AG', 'Antigua And Barbuda', 1268),
(10, 'AR', 'Argentina', 54),
(11, 'AM', 'Armenia', 374),
(12, 'AW', 'Aruba', 297),
(13, 'AU', 'Australia', 61),
(14, 'AT', 'Austria', 43),
(15, 'AZ', 'Azerbaijan', 994),
(16, 'BS', 'Bahamas The', 1242),
(17, 'BH', 'Bahrain', 973),
(18, 'BD', 'Bangladesh', 880),
(19, 'BB', 'Barbados', 1246),
(20, 'BY', 'Belarus', 375),
(21, 'BE', 'Belgium', 32),
(22, 'BZ', 'Belize', 501),
(23, 'BJ', 'Benin', 229),
(24, 'BM', 'Bermuda', 1441),
(25, 'BT', 'Bhutan', 975),
(26, 'BO', 'Bolivia', 591),
(27, 'BA', 'Bosnia and Herzegovina', 387),
(28, 'BW', 'Botswana', 267),
(29, 'BV', 'Bouvet Island', 0),
(30, 'BR', 'Brazil', 55),
(31, 'IO', 'British Indian Ocean Territory', 246),
(32, 'BN', 'Brunei', 673),
(33, 'BG', 'Bulgaria', 359),
(34, 'BF', 'Burkina Faso', 226),
(35, 'BI', 'Burundi', 257),
(36, 'KH', 'Cambodia', 855),
(37, 'CM', 'Cameroon', 237),
(38, 'CA', 'Canada', 1),
(39, 'CV', 'Cape Verde', 238),
(40, 'KY', 'Cayman Islands', 1345),
(41, 'CF', 'Central African Republic', 236),
(42, 'TD', 'Chad', 235),
(43, 'CL', 'Chile', 56),
(44, 'CN', 'China', 86),
(45, 'CX', 'Christmas Island', 61),
(46, 'CC', 'Cocos (Keeling) Islands', 672),
(47, 'CO', 'Colombia', 57),
(48, 'KM', 'Comoros', 269),
(49, 'CG', 'Congo', 242),
(50, 'CD', 'Congo The Democratic Republic Of The', 242),
(51, 'CK', 'Cook Islands', 682),
(52, 'CR', 'Costa Rica', 506),
(53, 'CI', 'Cote D''Ivoire (Ivory Coast)', 225),
(54, 'HR', 'Croatia (Hrvatska)', 385),
(55, 'CU', 'Cuba', 53),
(56, 'CY', 'Cyprus', 357),
(57, 'CZ', 'Czech Republic', 420),
(58, 'DK', 'Denmark', 45),
(59, 'DJ', 'Djibouti', 253),
(60, 'DM', 'Dominica', 1767),
(61, 'DO', 'Dominican Republic', 1809),
(62, 'TP', 'East Timor', 670),
(63, 'EC', 'Ecuador', 593),
(64, 'EG', 'Egypt', 20),
(65, 'SV', 'El Salvador', 503),
(66, 'GQ', 'Equatorial Guinea', 240),
(67, 'ER', 'Eritrea', 291),
(68, 'EE', 'Estonia', 372),
(69, 'ET', 'Ethiopia', 251),
(70, 'XA', 'External Territories of Australia', 61),
(71, 'FK', 'Falkland Islands', 500),
(72, 'FO', 'Faroe Islands', 298),
(73, 'FJ', 'Fiji Islands', 679),
(74, 'FI', 'Finland', 358),
(75, 'FR', 'France', 33),
(76, 'GF', 'French Guiana', 594),
(77, 'PF', 'French Polynesia', 689),
(78, 'TF', 'French Southern Territories', 0),
(79, 'GA', 'Gabon', 241),
(80, 'GM', 'Gambia The', 220),
(81, 'GE', 'Georgia', 995),
(82, 'DE', 'Germany', 49),
(83, 'GH', 'Ghana', 233),
(84, 'GI', 'Gibraltar', 350),
(85, 'GR', 'Greece', 30),
(86, 'GL', 'Greenland', 299),
(87, 'GD', 'Grenada', 1473),
(88, 'GP', 'Guadeloupe', 590),
(89, 'GU', 'Guam', 1671),
(90, 'GT', 'Guatemala', 502),
(91, 'XU', 'Guernsey and Alderney', 44),
(92, 'GN', 'Guinea', 224),
(93, 'GW', 'Guinea-Bissau', 245),
(94, 'GY', 'Guyana', 592),
(95, 'HT', 'Haiti', 509),
(96, 'HM', 'Heard and McDonald Islands', 0),
(97, 'HN', 'Honduras', 504),
(98, 'HK', 'Hong Kong S.A.R.', 852),
(99, 'HU', 'Hungary', 36),
(100, 'IS', 'Iceland', 354),
(101, 'IN', 'India', 91),
(102, 'ID', 'Indonesia', 62),
(103, 'IR', 'Iran', 98),
(104, 'IQ', 'Iraq', 964),
(105, 'IE', 'Ireland', 353),
(106, 'IL', 'Israel', 972),
(107, 'IT', 'Italy', 39),
(108, 'JM', 'Jamaica', 1876),
(109, 'JP', 'Japan', 81),
(110, 'XJ', 'Jersey', 44),
(111, 'JO', 'Jordan', 962),
(112, 'KZ', 'Kazakhstan', 7),
(113, 'KE', 'Kenya', 254),
(114, 'KI', 'Kiribati', 686),
(115, 'KP', 'Korea North', 850),
(116, 'KR', 'Korea South', 82),
(117, 'KW', 'Kuwait', 965),
(118, 'KG', 'Kyrgyzstan', 996),
(119, 'LA', 'Laos', 856),
(120, 'LV', 'Latvia', 371),
(121, 'LB', 'Lebanon', 961),
(122, 'LS', 'Lesotho', 266),
(123, 'LR', 'Liberia', 231),
(124, 'LY', 'Libya', 218),
(125, 'LI', 'Liechtenstein', 423),
(126, 'LT', 'Lithuania', 370),
(127, 'LU', 'Luxembourg', 352),
(128, 'MO', 'Macau S.A.R.', 853),
(129, 'MK', 'Macedonia', 389),
(130, 'MG', 'Madagascar', 261),
(131, 'MW', 'Malawi', 265),
(132, 'MY', 'Malaysia', 60),
(133, 'MV', 'Maldives', 960),
(134, 'ML', 'Mali', 223),
(135, 'MT', 'Malta', 356),
(136, 'XM', 'Man (Isle of)', 44),
(137, 'MH', 'Marshall Islands', 692),
(138, 'MQ', 'Martinique', 596),
(139, 'MR', 'Mauritania', 222),
(140, 'MU', 'Mauritius', 230),
(141, 'YT', 'Mayotte', 269),
(142, 'MX', 'Mexico', 52),
(143, 'FM', 'Micronesia', 691),
(144, 'MD', 'Moldova', 373),
(145, 'MC', 'Monaco', 377),
(146, 'MN', 'Mongolia', 976),
(147, 'MS', 'Montserrat', 1664),
(148, 'MA', 'Morocco', 212),
(149, 'MZ', 'Mozambique', 258),
(150, 'MM', 'Myanmar', 95),
(151, 'NA', 'Namibia', 264),
(152, 'NR', 'Nauru', 674),
(153, 'NP', 'Nepal', 977),
(154, 'AN', 'Netherlands Antilles', 599),
(155, 'NL', 'Netherlands The', 31),
(156, 'NC', 'New Caledonia', 687),
(157, 'NZ', 'New Zealand', 64),
(158, 'NI', 'Nicaragua', 505),
(159, 'NE', 'Niger', 227),
(160, 'NG', 'Nigeria', 234),
(161, 'NU', 'Niue', 683),
(162, 'NF', 'Norfolk Island', 672),
(163, 'MP', 'Northern Mariana Islands', 1670),
(164, 'NO', 'Norway', 47),
(165, 'OM', 'Oman', 968),
(166, 'PK', 'Pakistan', 92),
(167, 'PW', 'Palau', 680),
(168, 'PS', 'Palestinian Territory Occupied', 970),
(169, 'PA', 'Panama', 507),
(170, 'PG', 'Papua new Guinea', 675),
(171, 'PY', 'Paraguay', 595),
(172, 'PE', 'Peru', 51),
(173, 'PH', 'Philippines', 63),
(174, 'PN', 'Pitcairn Island', 0),
(175, 'PL', 'Poland', 48),
(176, 'PT', 'Portugal', 351),
(177, 'PR', 'Puerto Rico', 1787),
(178, 'QA', 'Qatar', 974),
(179, 'RE', 'Reunion', 262),
(180, 'RO', 'Romania', 40),
(181, 'RU', 'Russia', 70),
(182, 'RW', 'Rwanda', 250),
(183, 'SH', 'Saint Helena', 290),
(184, 'KN', 'Saint Kitts And Nevis', 1869),
(185, 'LC', 'Saint Lucia', 1758),
(186, 'PM', 'Saint Pierre and Miquelon', 508),
(187, 'VC', 'Saint Vincent And The Grenadines', 1784),
(188, 'WS', 'Samoa', 684),
(189, 'SM', 'San Marino', 378),
(190, 'ST', 'Sao Tome and Principe', 239),
(191, 'SA', 'Saudi Arabia', 966),
(192, 'SN', 'Senegal', 221),
(193, 'RS', 'Serbia', 381),
(194, 'SC', 'Seychelles', 248),
(195, 'SL', 'Sierra Leone', 232),
(196, 'SG', 'Singapore', 65),
(197, 'SK', 'Slovakia', 421),
(198, 'SI', 'Slovenia', 386),
(199, 'XG', 'Smaller Territories of the UK', 44),
(200, 'SB', 'Solomon Islands', 677),
(201, 'SO', 'Somalia', 252),
(202, 'ZA', 'South Africa', 27),
(203, 'GS', 'South Georgia', 0),
(204, 'SS', 'South Sudan', 211),
(205, 'ES', 'Spain', 34),
(206, 'LK', 'Sri Lanka', 94),
(207, 'SD', 'Sudan', 249),
(208, 'SR', 'Suriname', 597),
(209, 'SJ', 'Svalbard And Jan Mayen Islands', 47),
(210, 'SZ', 'Swaziland', 268),
(211, 'SE', 'Sweden', 46),
(212, 'CH', 'Switzerland', 41),
(213, 'SY', 'Syria', 963),
(214, 'TW', 'Taiwan', 886),
(215, 'TJ', 'Tajikistan', 992),
(216, 'TZ', 'Tanzania', 255),
(217, 'TH', 'Thailand', 66),
(218, 'TG', 'Togo', 228),
(219, 'TK', 'Tokelau', 690),
(220, 'TO', 'Tonga', 676),
(221, 'TT', 'Trinidad And Tobago', 1868),
(222, 'TN', 'Tunisia', 216),
(223, 'TR', 'Turkey', 90),
(224, 'TM', 'Turkmenistan', 7370),
(225, 'TC', 'Turks And Caicos Islands', 1649),
(226, 'TV', 'Tuvalu', 688),
(227, 'UG', 'Uganda', 256),
(228, 'UA', 'Ukraine', 380),
(229, 'AE', 'United Arab Emirates', 971),
(230, 'GB', 'United Kingdom', 44),
(231, 'US', 'United States', 1),
(232, 'UM', 'United States Minor Outlying Islands', 1),
(233, 'UY', 'Uruguay', 598),
(234, 'UZ', 'Uzbekistan', 998),
(235, 'VU', 'Vanuatu', 678),
(236, 'VA', 'Vatican City State (Holy See)', 39),
(237, 'VE', 'Venezuela', 58),
(238, 'VN', 'Vietnam', 84),
(239, 'VG', 'Virgin Islands (British)', 1284),
(240, 'VI', 'Virgin Islands (US)', 1340),
(241, 'WF', 'Wallis And Futuna Islands', 681),
(242, 'EH', 'Western Sahara', 212),
(243, 'YE', 'Yemen', 967),
(244, 'YU', 'Yugoslavia', 38),
(245, 'ZM', 'Zambia', 260),
(246, 'ZW', 'Zimbabwe', 263);

-- --------------------------------------------------------

--
-- Table structure for table `deduction`
--

CREATE TABLE `deduction` (
  `deduction_id` int(11) NOT NULL,
  `emp_id` int(10) NOT NULL,
  `deduction_type` tinyint(1) NOT NULL DEFAULT '0',
  `amount` double NOT NULL,
  `note` text NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE `department` (
  `deptId` int(11) NOT NULL,
  `managerId` int(10) NOT NULL,
  `deptParentId` int(10) NOT NULL,
  `deptName` varchar(40) NOT NULL,
  `location` varchar(30) NOT NULL,
  `deptDesc` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`deptId`, `managerId`, `deptParentId`, `deptName`, `location`, `deptDesc`) VALUES
(1, 1, 0, 'Administration', '', 'Administration'),
(2, 39, 1, 'Customer Relation ', '', '...'),
(3, 41, 2, 'CRD', 'second floor', ''),
(4, 45, 0, 'Techinical', 'north', '');

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `empId` int(11) NOT NULL,
  `deptId` int(10) NOT NULL,
  `jobId` int(10) NOT NULL,
  `catId` int(10) NOT NULL,
  `typeId` int(10) NOT NULL,
  `firstName` varchar(25) NOT NULL,
  `lastName` varchar(20) NOT NULL,
  `dob` date DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `cellPhone` int(17) NOT NULL DEFAULT '0',
  `officePhone` int(17) NOT NULL DEFAULT '0',
  `image` varchar(100) DEFAULT 'default_image.png',
  `gender` int(1) NOT NULL DEFAULT '5',
  `active` int(1) NOT NULL DEFAULT '0',
  `ssn` varchar(20) DEFAULT NULL,
  `empCode` varchar(12) DEFAULT NULL,
  `lastLogin` date DEFAULT NULL,
  `dateSignup` date DEFAULT NULL,
  `createDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='employee table';

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`empId`, `deptId`, `jobId`, `catId`, `typeId`, `firstName`, `lastName`, `dob`, `email`, `cellPhone`, `officePhone`, `image`, `gender`, `active`, `ssn`, `empCode`, `lastLogin`, `dateSignup`, `createDate`) VALUES
(1, 1, 0, 0, 0, 'Admin', 'admin', '2016-10-19', 'admin@admin.com', 0, 0, '', 1, 1, '', 'TL_452082010', '0000-00-00', '0000-00-00', '2017-08-23 19:43:42'),
(2, 1, 2, 0, 0, 'Rashed', 'khan', '2016-05-18', 'rashed@constantmd.com', 232323423, 2147483647, 'default_image.jpg', 1, 1, '33ddd23233', 'TL_452082011', '0000-00-00', '0000-00-00', '2017-08-23 19:48:24'),
(3, 1, 2, 2, 1, 'Arif', 'khan', '2017-08-09', 'arif@gmail.com', 23235, 234234, 'default_image.jpg', 1, 1, '123421312', 'TL_452082014', '2017-08-24', '2017-08-24', '2017-08-26 19:35:21'),
(38, 1, 2, 1, 2, 'Rakib', 'Hasan', '2001-01-04', 'rakib@gmail.com', 345, 345, 'default_image.jpg', 2, 1, '45', 'TL_452082015', NULL, NULL, '2017-09-19 18:49:43'),
(39, 1, 2, 1, 1, 'Sohag', 'Hossain', '2012-01-04', 'sohag@gmail.com', 45, 345, 'default_image.jpg', 1, 1, '345345', 'TL_452082017', NULL, NULL, '2017-09-19 18:51:08'),
(40, 1, 1, 1, 1, 'Sowrab', 'ALi', '1995-01-04', 'sowrab@gmail.com', 35345, 35, 'default_image.jpg', 1, 0, '34', 'TL_04425833', NULL, NULL, '2017-09-25 17:33:24'),
(41, 1, 1, 1, 2, 'Faisal', 'khan ', '1995-01-10', 'faisal@gmail.com', 12121212, 45454, 'default_image.jpg', 1, 1, '35345', 'TL_101021919', NULL, NULL, '2017-10-20 18:19:11'),
(42, 3, 3, 1, 2, 'Abdus', 'Salam', '2001-01-04', 'salam@gmail.com', 3333, 3333, 'default_image.jpg', 1, 1, '4334', 'TL_4581052', NULL, NULL, '2017-11-07 21:52:22'),
(43, 3, 3, 2, 3, 'Bayzid', 'Islam', '2001-01-04', 'bayzid@gmail.com', 456456, 456, 'default_image.jpg', 1, 1, '534534', 'TL_44131038', NULL, NULL, '2017-11-13 17:38:18'),
(44, 3, 3, 1, 1, 'Khalek', 'Mia', '1994-01-10', 'khalek@gmail.com', 1472583694, 41578874, 'default_image.jpg', 2, 1, 'SSSSS4444', 'TL_101081119', NULL, NULL, '2017-12-07 19:19:26'),
(45, 1, 2, 2, 1, 'Sorif', 'Hossain', '1995-01-10', 'sorif@gmail.com', 147258369, 45345345, 'default_image.jpg', 1, 1, 'S5455', 'TL_101281124', NULL, NULL, '2017-12-07 19:24:02'),
(46, 1, 2, 3, 2, 'Nazmus', 'Sakib', '1994-01-05', 'sakib@gmail.com', 123789456, 345345345, 'default_image.jpg', 1, 1, '4534534', 'TL_50181125', NULL, NULL, '2017-12-07 19:25:56');

-- --------------------------------------------------------

--
-- Table structure for table `emp_address`
--

CREATE TABLE `emp_address` (
  `addressId` int(10) NOT NULL,
  `address1` varchar(100) NOT NULL,
  `address2` varchar(100) NOT NULL,
  `city` varchar(25) NOT NULL,
  `state` varchar(20) NOT NULL,
  `zip` int(6) NOT NULL DEFAULT '0',
  `countryId` int(10) NOT NULL DEFAULT '0',
  `empId` int(10) NOT NULL,
  `createDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `emp_address`
--

INSERT INTO `emp_address` (`addressId`, `address1`, `address2`, `city`, `state`, `zip`, `countryId`, `empId`, `createDate`) VALUES
(1, 'sdf', 'sdf', 'dddd', 'sdf', 3455, 0, 36, '2017-09-19 18:11:31'),
(2, 'sdf', 'sdf', 'sdf', 'sd', 35, 18, 37, '2017-09-19 18:39:01'),
(3, 'sdf', 'sdf', 's', 'd', 34, 0, 0, '2017-09-19 18:40:35'),
(4, 'sf', 'sfd', 'ddfsfd', 'sdf', 44, 0, 38, '2017-09-19 18:49:43'),
(5, 'sdf', 'sdf', 'dddd', 'dsf', 3455, 18, 39, '2017-09-19 18:51:08'),
(6, 'sdf', 'sdf', 'dddd', 'dsf', 345, 0, 40, '2017-09-25 17:33:24'),
(7, 'Banglabazar\n ', 'Banglabazar\n ', 'Barisal', 'Barisal', 8300, 0, 41, '2017-10-20 18:19:12'),
(8, 'sdf', 'sdf', 'dd', 'd', 434, 18, 42, '2017-11-07 21:52:22'),
(9, 'sdfsdfsdf', 'sdfsdf', 'fff', 'ssss', 3334, 18, 43, '2017-11-13 17:38:19'),
(10, 'Mirpur-10, Dhaka\n	', 'Bangladesh', 'dhaka', 'Dhaka', 1216, 18, 44, '2017-12-07 19:19:27'),
(11, 'Charfassion	', 'Dhaka', 'dhka', 'dhk', 1612, 18, 45, '2017-12-07 19:24:03'),
(12, 'Dhaka	', 'Mirpur', 'dhk', 'dkh', 1216, 18, 46, '2017-12-07 19:25:57');

-- --------------------------------------------------------

--
-- Table structure for table `emp_category`
--

CREATE TABLE `emp_category` (
  `catId` int(11) NOT NULL,
  `catName` varchar(30) NOT NULL,
  `catDesc` varchar(100) NOT NULL,
  `note` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `emp_category`
--

INSERT INTO `emp_category` (`catId`, `catName`, `catDesc`, `note`) VALUES
(1, 'A', 'sdf', 'dsfsdf'),
(2, 'B', 'sdf', 'sdf'),
(3, 'e', 'sss', NULL),
(4, 'National', '.......', NULL),
(5, 'International', '...', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `emp_type`
--

CREATE TABLE `emp_type` (
  `typeId` int(11) NOT NULL,
  `typeName` varchar(33) NOT NULL,
  `note` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `emp_type`
--

INSERT INTO `emp_type` (`typeId`, `typeName`, `note`) VALUES
(1, 'Operational and Administrative', 'sfsdf'),
(2, 'Managerial & Professional (M&P):', 'fvv'),
(3, 'Executive  Senior Administrative ', 'sdff'),
(4, 'Test', 'aaa'),
(5, 'Contracted', 'ddd');

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE `event` (
  `event_id` int(11) NOT NULL,
  `dept_id` int(11) NOT NULL,
  `event_date` int(11) NOT NULL,
  `event_type` int(11) NOT NULL,
  `event_desc` int(11) NOT NULL,
  `posted_by` varchar(30) NOT NULL,
  `date_posted` date NOT NULL,
  `expiry_date` date NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `holidays`
--

CREATE TABLE `holidays` (
  `hoiday_id` int(11) NOT NULL,
  `emp_id` int(10) NOT NULL,
  `date_holiday` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hourly`
--

CREATE TABLE `hourly` (
  `hour_id` int(11) NOT NULL,
  `hourly_rate` int(10) NOT NULL,
  `emp_id` int(10) NOT NULL,
  `note` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `increment`
--

CREATE TABLE `increment` (
  `increment_id` int(11) NOT NULL,
  `amount` double NOT NULL,
  `emp_id` int(10) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `job`
--

CREATE TABLE `job` (
  `jobId` int(11) NOT NULL,
  `deptId` int(11) NOT NULL,
  `jobTitle` varchar(100) NOT NULL,
  `jobDesc` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `job`
--

INSERT INTO `job` (`jobId`, `deptId`, `jobTitle`, `jobDesc`) VALUES
(1, 1, 'Managin Director', 'Managing Director'),
(2, 1, 'Admin', 'Admin'),
(3, 3, 'Executive', 'sddd'),
(4, 4, 'Software Engineer', '....');

-- --------------------------------------------------------

--
-- Table structure for table `locks`
--

CREATE TABLE `locks` (
  `lock_id` int(11) NOT NULL,
  `emp_id` int(10) NOT NULL,
  `lock_date` date NOT NULL,
  `reason_lock` text NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `message_id` int(11) NOT NULL,
  `emp_id` int(10) NOT NULL,
  `message` text NOT NULL,
  `date_posted` int(11) NOT NULL,
  `posted_by` varchar(30) NOT NULL,
  `num_views` int(10) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `payroll`
--

CREATE TABLE `payroll` (
  `payrollId` int(11) NOT NULL,
  `empId` int(10) NOT NULL,
  `date` date NOT NULL,
  `startDate` date DEFAULT NULL,
  `endDate` date DEFAULT NULL,
  `hoursWorked` int(6) NOT NULL,
  `grossPay` double NOT NULL,
  `deductions` double NOT NULL,
  `netPay` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payroll`
--

INSERT INTO `payroll` (`payrollId`, `empId`, `date`, `startDate`, `endDate`, `hoursWorked`, `grossPay`, `deductions`, `netPay`) VALUES
(1, 46, '2016-12-31', NULL, NULL, 100, 4444, 100, 4344),
(10, 46, '2017-01-03', NULL, NULL, 1000, 3333, 1000, 2333),
(20, 46, '2017-11-12', NULL, NULL, 150, 3333, 333, 3000);

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE `project` (
  `projectId` int(11) NOT NULL,
  `deptId` int(10) NOT NULL,
  `projectTitle` varchar(100) NOT NULL,
  `projectDesc` text NOT NULL,
  `hoursWorked` int(10) NOT NULL DEFAULT '0',
  `active` int(1) NOT NULL DEFAULT '0',
  `managerId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`projectId`, `deptId`, `projectTitle`, `projectDesc`, `hoursWorked`, `active`, `managerId`) VALUES
(1, 2, 'test', 'test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test ', 20, 1, NULL),
(2, 2, 'A', 'sdfsf', 0, 0, 41),
(3, 2, 'test2', 'fdf	', 0, 0, 41),
(4, 3, 'test3', 'fsdf', 0, 1, 39),
(5, 2, 'ssss', 'sdf', 0, 1, 38),
(6, 2, 'ggg', 'sdf', 0, 1, 41),
(7, 4, 'Robi Network', '', 0, 1, 43),
(8, 4, 'gp', '', 0, 1, 45);

-- --------------------------------------------------------

--
-- Table structure for table `project_details`
--

CREATE TABLE `project_details` (
  `projectDetailsId` int(11) NOT NULL,
  `empId` int(11) NOT NULL,
  `projectId` int(11) DEFAULT NULL,
  `joinDate` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `project_details`
--

INSERT INTO `project_details` (`projectDetailsId`, `empId`, `projectId`, `joinDate`) VALUES
(1, 39, 4, NULL),
(2, 41, 6, '2017-11-08'),
(3, 41, 1, '2017-11-13'),
(4, 44, 6, '2017-12-08');

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `roleId` int(11) NOT NULL,
  `roleName` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`roleId`, `roleName`) VALUES
(1, 'Admin'),
(2, 'Editor'),
(3, 'Employee'),
(4, 'HR Manager');

-- --------------------------------------------------------

--
-- Table structure for table `salary`
--

CREATE TABLE `salary` (
  `salaryId` int(11) NOT NULL,
  `empId` int(10) NOT NULL,
  `year` int(5) NOT NULL,
  `note` text NOT NULL,
  `amount` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `salary`
--

INSERT INTO `salary` (`salaryId`, `empId`, `year`, `note`, `amount`) VALUES
(1, 46, 2017, '', 3333),
(2, 45, 2017, '', 12000);

-- --------------------------------------------------------

--
-- Table structure for table `sickdays`
--

CREATE TABLE `sickdays` (
  `sickday_id` int(11) NOT NULL,
  `emp_id` int(10) NOT NULL,
  `date_sick` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `temp_session`
--

CREATE TABLE `temp_session` (
  `userSessionId` int(11) NOT NULL,
  `userId` int(11) DEFAULT NULL,
  `startLogin` date DEFAULT NULL,
  `isLogin` int(1) DEFAULT '1',
  `logOutTime` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `temp_session`
--

INSERT INTO `temp_session` (`userSessionId`, `userId`, `startLogin`, `isLogin`, `logOutTime`) VALUES
(24, 5, '2017-11-14', 1, NULL),
(25, 5, '2017-11-14', 1, NULL),
(26, 1, '2017-11-14', 1, NULL),
(27, 1, '2017-11-14', 1, NULL),
(28, 5, '2017-11-14', 1, NULL),
(29, 5, '2017-11-14', 1, NULL),
(30, 5, '2017-11-14', 1, NULL),
(31, 5, '2017-11-14', 1, NULL),
(32, 5, '2017-11-14', 1, NULL),
(33, 1, '2017-11-14', 1, NULL),
(34, 1, '2017-11-14', 1, NULL),
(35, 1, '2017-11-14', 1, NULL),
(36, 1, '2017-11-14', 1, NULL),
(37, 1, '2017-11-14', 1, NULL),
(38, 1, '2017-11-14', 1, NULL),
(39, 1, '2017-11-14', 1, NULL),
(40, 1, '2017-11-14', 1, NULL),
(41, 1, '2017-11-14', 1, NULL),
(42, 1, '2017-11-14', 1, NULL),
(43, 1, '2017-11-14', 1, NULL),
(44, 1, '2017-11-14', 1, NULL),
(45, 1, '2017-11-14', 1, NULL),
(46, 1, '2017-11-14', 1, NULL),
(47, 1, '2017-11-14', 1, NULL),
(48, 1, '2017-11-14', 1, NULL),
(49, 1, '2017-11-14', 1, NULL),
(50, 1, '2017-11-14', 1, NULL),
(51, 1, '2017-12-08', 1, NULL),
(52, 1, '2017-12-08', 1, NULL),
(53, 1, '2017-12-08', 1, NULL),
(54, 1, '2017-12-08', 1, NULL),
(55, 1, '2017-12-08', 1, NULL),
(56, 1, '2017-12-08', 1, NULL),
(57, 1, '2017-12-08', 1, NULL),
(58, 1, '2017-12-08', 1, NULL),
(59, 1, '2017-12-08', 1, NULL),
(60, 1, '2017-12-08', 1, NULL),
(61, 1, '2017-12-08', 1, NULL),
(62, 1, '2017-12-08', 1, NULL),
(63, 7, '2017-12-08', 1, NULL),
(64, 1, '2017-12-08', 1, NULL),
(65, 1, '2017-12-08', 1, NULL),
(66, 1, '2017-12-08', 1, NULL),
(67, 1, '2017-12-08', 1, NULL),
(68, 1, '2017-12-08', 1, NULL),
(69, 1, '2017-12-08', 1, NULL),
(70, 1, '2017-12-08', 1, NULL),
(71, 1, '2017-12-08', 1, NULL),
(72, 1, '2017-12-08', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `timesheet`
--

CREATE TABLE `timesheet` (
  `timesheetId` int(11) NOT NULL,
  `empId` int(10) NOT NULL,
  `projectId` int(10) NOT NULL,
  `attendDate` date NOT NULL,
  `checkIn` time NOT NULL,
  `checkOut` time DEFAULT NULL,
  `workDesc` text NOT NULL,
  `checked` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `timesheet`
--

INSERT INTO `timesheet` (`timesheetId`, `empId`, `projectId`, `attendDate`, `checkIn`, `checkOut`, `workDesc`, `checked`) VALUES
(1, 3, 1, '2017-10-24', '16:11:53', '20:11:53', '', 0),
(2, 39, 1, '2017-10-24', '05:05:13', '17:07:13', 'test', 0),
(3, 2, 1, '2017-10-24', '03:03:42', '18:06:42', 'dsdf', 1),
(4, 40, 1, '2017-10-24', '04:10:36', '19:05:36', '', 1),
(5, 41, 1, '2017-10-24', '07:03:06', '19:05:06', '', 0),
(6, 42, 1, '2017-11-13', '10:10:42', '19:10:42', 'safsdfsdaf sdaf sdaf sdf sadf s fsd f', 1),
(7, 46, 1, '2017-12-08', '08:03:52', '18:05:52', 'sdfsdfsdfsdf', 1),
(8, 45, 4, '2017-12-08', '02:02:17', '02:02:17', '', 1),
(9, 46, 2, '2017-12-09', '10:15:46', '19:04:24', 'test', 0),
(12, 45, 2, '2017-12-09', '10:01:50', '20:05:11', 'teswt', 0),
(13, 45, 1, '2017-12-10', '18:01:09', '19:01:30', '.....', 0),
(14, 45, 2, '2017-12-11', '09:04:24', NULL, '...........', 0),
(15, 39, 6, '2017-12-11', '10:04:50', NULL, '...........', 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `userId` int(11) NOT NULL,
  `roleId` int(10) NOT NULL,
  `empId` int(10) NOT NULL,
  `password` varchar(50) NOT NULL,
  `active` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`userId`, `roleId`, `empId`, `password`, `active`) VALUES
(1, 1, 1, 'e10adc3949ba59abbe56e057f20f883e', 1),
(5, 3, 43, 'e10adc3949ba59abbe56e057f20f883e', 1),
(6, 3, 46, 'e10adc3949ba59abbe56e057f20f883e', 1),
(7, 4, 41, '4607e782c4d86fd5364d7e4508bb10d9', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bonus`
--
ALTER TABLE `bonus`
  ADD PRIMARY KEY (`bounsId`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `deduction`
--
ALTER TABLE `deduction`
  ADD PRIMARY KEY (`deduction_id`);

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`deptId`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`empId`);

--
-- Indexes for table `emp_address`
--
ALTER TABLE `emp_address`
  ADD PRIMARY KEY (`addressId`);

--
-- Indexes for table `emp_category`
--
ALTER TABLE `emp_category`
  ADD PRIMARY KEY (`catId`);

--
-- Indexes for table `emp_type`
--
ALTER TABLE `emp_type`
  ADD PRIMARY KEY (`typeId`);

--
-- Indexes for table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`event_id`);

--
-- Indexes for table `holidays`
--
ALTER TABLE `holidays`
  ADD PRIMARY KEY (`hoiday_id`);

--
-- Indexes for table `hourly`
--
ALTER TABLE `hourly`
  ADD PRIMARY KEY (`hour_id`);

--
-- Indexes for table `increment`
--
ALTER TABLE `increment`
  ADD PRIMARY KEY (`increment_id`);

--
-- Indexes for table `job`
--
ALTER TABLE `job`
  ADD PRIMARY KEY (`jobId`);

--
-- Indexes for table `locks`
--
ALTER TABLE `locks`
  ADD PRIMARY KEY (`lock_id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`message_id`);

--
-- Indexes for table `payroll`
--
ALTER TABLE `payroll`
  ADD PRIMARY KEY (`payrollId`);

--
-- Indexes for table `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`projectId`);

--
-- Indexes for table `project_details`
--
ALTER TABLE `project_details`
  ADD PRIMARY KEY (`projectDetailsId`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`roleId`);

--
-- Indexes for table `salary`
--
ALTER TABLE `salary`
  ADD PRIMARY KEY (`salaryId`);

--
-- Indexes for table `sickdays`
--
ALTER TABLE `sickdays`
  ADD PRIMARY KEY (`sickday_id`);

--
-- Indexes for table `temp_session`
--
ALTER TABLE `temp_session`
  ADD PRIMARY KEY (`userSessionId`);

--
-- Indexes for table `timesheet`
--
ALTER TABLE `timesheet`
  ADD PRIMARY KEY (`timesheetId`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`userId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bonus`
--
ALTER TABLE `bonus`
  MODIFY `bounsId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=247;
--
-- AUTO_INCREMENT for table `deduction`
--
ALTER TABLE `deduction`
  MODIFY `deduction_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `department`
--
ALTER TABLE `department`
  MODIFY `deptId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `empId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `emp_address`
--
ALTER TABLE `emp_address`
  MODIFY `addressId` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `emp_category`
--
ALTER TABLE `emp_category`
  MODIFY `catId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `emp_type`
--
ALTER TABLE `emp_type`
  MODIFY `typeId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `event`
--
ALTER TABLE `event`
  MODIFY `event_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `holidays`
--
ALTER TABLE `holidays`
  MODIFY `hoiday_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `hourly`
--
ALTER TABLE `hourly`
  MODIFY `hour_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `increment`
--
ALTER TABLE `increment`
  MODIFY `increment_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `job`
--
ALTER TABLE `job`
  MODIFY `jobId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `locks`
--
ALTER TABLE `locks`
  MODIFY `lock_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `message_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `payroll`
--
ALTER TABLE `payroll`
  MODIFY `payrollId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `project`
--
ALTER TABLE `project`
  MODIFY `projectId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `project_details`
--
ALTER TABLE `project_details`
  MODIFY `projectDetailsId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `roleId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `salary`
--
ALTER TABLE `salary`
  MODIFY `salaryId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `sickdays`
--
ALTER TABLE `sickdays`
  MODIFY `sickday_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `temp_session`
--
ALTER TABLE `temp_session`
  MODIFY `userSessionId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;
--
-- AUTO_INCREMENT for table `timesheet`
--
ALTER TABLE `timesheet`
  MODIFY `timesheetId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `userId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
